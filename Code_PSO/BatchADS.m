function BatchADS(D,modelType,ID,dt,tstop,delay,freq,celsius,v_init)
%% stable
len=50000;%Stable[um]
segdensity=100;%segment length [um]
initialdel=0; % pulse delay
dura=0.1; % duration of pulse [ms]
dt=dt;
%% Togglable
if ID>=1
    type =modelType; % fiber type 1:Sundt 2:Tigerholm 3:Rattay
    ParticleID = ID;
    call_neuron_MATLAB('ActivityDependentSlowing.hoc',ParticleID,segdensity,dt,tstop,len,D,type,dura,initialdel,delay,freq,celsius,v_init);
else
    for i=1:ID
        type =modelType; % fiber type 1:Sundt 2:Tigerholm 3:Rattay
        ParticleID = i;
        call_neuron_MATLAB('ActivityDependentSlowing.hoc',ParticleID,segdensity,dt,tstop,len,D,type,dura,initialdel,v_init);
    end
end

addpath('..');
D=[0.5 1 1.5];
dt=0.01;
tstop=1000;
modelType=5;

cd(['Elect'])
latestFile=getlatestfile('.');
j=1;
while ~strcmp(['Swarm' num2str(j) '.mat'],latestFile) & ~strcmp(['Outcomes' num2str(j) '.mat'],latestFile)
    j=j+1;
end
load(['Swarm' num2str(j) '.mat'])
val=max(sum(bp));
cd('..')

ParticleIDs=[find(sum(bp)==val)];
freq=[0.5 1 2 4];
temp=[37];
%% Parallelize CV
ids=[];
for ID=1
    ParticleID=ParticleIDs(ID);
    CS = load(['ConductanceSets/Particle' num2str(ParticleID) '.dat']);
    v_init = CS(end);
    for f=1:length(freq)
        for q=1:length(D)
            for tem=1:length(temp)
                %             disp(D)
                %                             p=1
                %                             f=4
                %                             q=2
                %                             tem=2
                folder = ['BatchADS' num2str(q) '_' num2str(f) '_' num2str(tem)];
                name = 'bjt20';
                dir='ADS';
                %write tester.slurm
                fid = fopen([dir '/' folder '.slurm'],'w');
                fprintf(fid,'#!/bin/bash\n');
                fprintf(fid,'#\n');
                fprintf(fid,'#SBATCH --output=Outw.out\n');
                fprintf(fid,'#SBATCH --open-mode=append\n');
                fprintf(fid,'#SBATCH --job-name=Testing\n');
                fprintf(fid,'#SBATCH --mem-per-cpu=16G\n');
                fprintf(fid,'#SBATCH --error=Er.err\n');
                %             fprintf(fid,'#SBATCH --exclude=dcc-peterchev-01,dcc-peterchev-02,dcc-peterchev-03,dcc-peterchev-04,dcc-peterchev-05,dcc-peterchev-06,dcc-peterchev-07,dcc-peterchev-08,dcc-peterchev-09,dcc-peterchev-10,dcc-peterchev-11,dcc-peterchev-12,dcc-wmglab-01,dcc-wmglab-02,dcc-wmglab-03,dcc-wmglab-04,dcc-wmglab-05,dcc-wmglab-15,dcc-wmglab-16,dcc-wmglab-17,dcc-wmglab-18,dcc-wmglab-19,dcc-wmglab-20,dcc-wmglab-21,dcc-wmglab-22,dcc-wmglab-23,dcc-wmglab-24,dcc-wmglab-25,dcc-wmglab-26,dcc-wmglab-27,dcc-wmglab-28,dcc-wmglab-29,dcc-wmglab-30,dcc-wmglab-31,dcc-wmglab-32,dcc-wmglab-33,dcc-wmglab-34,dcc-wmglab-35,dcc-wmglab-36,dcc-wmglab-37,dcc-wmglab-38,dcc-wmglab-39,dcc-wmglab-40,dcc-wmglab-41,dcc-wmglab-42,dcc-wmglab-43\n');
                fprintf(fid,'#SBATCH -p wmglab\n\n');
                %     fprintf(fid,'cd %s\n\n',dir);
                %fprintf(fid,'sleep 30\n\n');
                fprintf(fid,['/opt/apps/rhel7/matlabR2019a/bin/matlab -nojvm -nodisplay -r'...
                    ' "addpath(''~/SPARC_Optimize''),D=' num2str(D(q))...
                    ',numDim=' num2str(ParticleID) ',dt=' num2str(dt) ',modelType=' num2str(modelType) ...
                    ',tstop=' num2str(tstop) ',delay=' num2str(1/freq(f)) ',freq=' num2str(freq(f))...
                    ',Temperature=' num2str(temp(tem)) ',v_init=' num2str(v_init) ',addpath(''./PSOFunctions''),BatchADS(D,modelType,numDim,dt,tstop,delay,freq,Temperature,v_init),exit"']);
                fclose(fid);
                %             %submit the job and catch the job id
                [MasterID,~] = evalc(sprintf('system(''sbatch %s/%s.slurm'')',dir,folder));
                ids= [ids str2double(MasterID(21:end-1))];
                %             ids=[ids BatchPP(IPI,D(q),modelType,ParticleID,dt,tstop)];
            end
        end
    end
end

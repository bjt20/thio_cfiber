%% Particle Swarm Optimization for cable model conductance optimization
% Written by: Tim Hoer, timothy.hoer@duke.edu
% Adapted by: Brandon Thio, bjt20@duke.edu
% May 22, 2018
%% REQUIRED FUNCTIONS AND FILES: (more details on each in README.txt)
% - initialize.m: [swarm, convergence] = initialize(dt, tstop, numParts, numDim, upper, lower, numOutcomes,modelType,PW,D,IPI,Super)
% - elect.m: [swarm_elect] = elect(swarm,numParts,numNbh,numDim,upper)
% - evaluate.m: [swarm, convergence] = evaluate(dt, tstop,swarm, numParts, numDim, upper, iteration, NumOutcomes,modelType,PW,D,IPI,Super)
% - passGo.m: [swarm] = passGo(swarm, numParts, numNbh, numDim, upper, lower, v_coeff)
% - particle.m
% - setupDirectories.m
%% OUTPUT:
% - files saved to directories setup in setupDirectories.m

%% Parameters
addpath('./Knowledge');
addpath('./PSOFunctions');

%global variables
global startingIteration upper lower numParts numDim

%PSO Variables
term_min = 1600; %minimum number of iterations per run
term_stability = 0;
numParts = 17; %number of particles for each dimension (number of conductances)
upper = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 2 0.7 -50]; %upper bound of each type of particle ie upper bound of each conductance
lower = [-6 -6 -6 -6 -6 -6 -6 -6 -6 -6 -6 -6 -6 -6 1 -1 -78];%lower bound of each type of particle is lower bound of each conductance no negative conductances or changed current direction


%% Set up parpool
numCPUs = feature('numCores'); % Get number of CPUs available
pc_storage_dir = fullfile('pc_temp_storage',getenv('SLURM_JOB_ID'));    % assign JobStorageLocation based on id of SLURM job that called this function   
mkdir(pc_storage_dir);
pc = parcluster('local');   
pc.JobStorageLocation = pc_storage_dir;
fprintf('Number of CPUs requested = %g\n',numCPUs);
poolobj = parpool(pc,numCPUs-1); % initialize pool of workers, i.e. CPUs in this node to assign tasks, leave 1 CPU to handle the overhead
numDim = 40; %number of swarm dimensions (sets of conductances)

%% Initialization
setupDirectories;

rng('shuffle');

%set_stimamp(stimamp, numNbh, numDim);%do not thing I need this
[swarm, convergence] = initialize(); %creates current swarm array (particles x dimensions)

%% Routine
iteration = startingIteration+1;
while (iteration <= (term_min + term_stability)) && (convergence == 0)
    % Update velocity and location of particles
    swarm = passGo(swarm,iteration); %update position and velocity
    
    % Evaluate the performance of particles in the swarm
    [swarm, convergence] = evaluate(swarm,iteration); %update fitness and reform neighborhood
    
    % move to next generation
    iteration = iteration + 1;
end

if convergence == 1
    fprintf('THE SWARM HAS CONVERGED.')
else
    fprintf('THE SWARM FAILED TO CONVERGE.')
end

% stop parpool
delete(poolobj);
rmdir(pc_storage_dir,'s'); % delete parfor temporary files

% exit
quit

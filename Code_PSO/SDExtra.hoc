// Created by Brandon Thio 7 February 2018
// create c-fibers based upon literature defined models
// set these parameters in MATLAB:
//		tstop, len, D, segdensity, type, initialdel, dura
load_file("cFiberBuilder.hoc")
load_file("balance.hoc")

objref fiber, f
objectvar stim,stim2
strdef name

// Run the simulation
proc stimulate(){

	maxMidVm=-10000 // max voltage at midnode
	Nan=0 // did Nan occur (logical)
	// Simulation Loop starts here
	while (t<tstop){	
		sec=0
		forsec fiber.sl{
			//Apply stimulation before end of pulsewidth
			if(t<dura){
				r=sqrt(dist*dist+fiber.section_coord.x(sec)*fiber.section_coord.x(sec))
				e_extracellular(0.5)=-1*amp/(4*3.14*sigma*r)
			}else{
				e_extracellular(0.5)=0
			}
			//detect AP 2mm from center node  int(fiber.nsegments/2+240)
			if((sec==fiber.nsegments-2) && v>-10){
				fire=1
			}
			// keep track of max voltage at center segment
			if(int(fiber.nsegments/2)==sec && v>maxMidVm){
				maxMidVm=v
			}
			// does NaN occur
			if(!(v>0)&&!(v<=0)){
				Nan=1
			}
			sec=sec+1
		}
		if (fire == 1) {
			break
		}
		fadvance()	      	 	// Advance simulation one time step	    
	}
	
	if(maxMidVm<-10&&Nan==0){//handels Nan and not firing
		centerFire=1//did not fire at center
	}
	if(Nan==1){
		fire=0
	}
	if(Nan==1&&start==1){
		fire=1
	}
	
}


proc run_nrn_script(){
//////////////////// VARIABLES /////////////////////
celsius = celsius
dist=0.00015

///////////////// Create Fibers ///////////////////////
fiber = new cFiberBuilder(D,len,type,celsius,50/6,ParticleID)
vinit=v_init

///////////////////////// STIMULATE AXON /////////////////////

t=0	     			    // Set time variable (t) to zero
low=0
fire=0
ct=0
high=1
///////////////////////Finds Upper bound//////////////////////
start=0
nope=0
finitialize(vinit)
fcurrent()
balance()
while(fire==0){
centerFire=0
amp=high
finitialize(vinit)
fcurrent()						// Sets all currents and other assigned values in accordance with current values of state variables.
t=0
stimulate()
if(fire==1){
	break
}
if(centerFire==1){
	// low=high
	high=high*2
}else{
	high=high/2
}
if(high<.000001){// less than 10nA probably doesn't matter
	break
}
if(ct>10){// break out after a certain time because most likely going to be very extreme values of current
	nope=1
	break
}
ct=ct+1
}

///////////////////////Find Thresh////////////////////
start=1
if(nope==0){
while(((high-low)/high)>0.01){
mid=(high+low)/2
amp=mid
finitialize(vinit)
fire=0
t=0
stimulate()
if(fire==1){
	high=mid
}else{
	low=mid
}
}
}else{
high=0
}
//////////////////// Print Threshold to File //////////////////
sprint(name,"SD/c_fiber_Thresh_%d_C_%d_D_%d_Duration_%d_Type_%d.dat",celsius,D*1000,dura*1000,type,ParticleID)
f=new File()
f.wopen(name)
f.printf("%f",high)
f.close()
}

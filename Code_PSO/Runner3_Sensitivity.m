%% Load in Base Data
function Runner3_Sensitivity(ID)
addpath('./PSOFunctions');
addpath('..');
cd(['Elect'])
latestFile=getlatestfile('.');
j=1;
while ~strcmp(['Swarm' num2str(j) '.mat'],latestFile) & ~strcmp(['Outcomes' num2str(j) '.mat'],latestFile)
    j=j+1;
end
load(['Swarm' num2str(j) '.mat'])
val=max(sum(bp));
cd('..')

ID2=[find(sum(bp)==val)];

modelType = 5;%4;
% list of pulse widths in ms
PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 2 10];
% list of diameters in um
D = [0.5 1 1.5 2];
% list of IPI in ms
IPI = 1:2:250;%;[13 23 51 101 250];
dt=0.01;
tstop=20;
data=load(['ConductanceSets/Particle' num2str(ID2) '.dat']);
if mod(ID,2)==1
    data(floor(ID/100))=data(floor(ID/100))/2;
else
    data(floor(ID/100))=data(floor(ID/100))+data(floor(ID/100))/2;
end
save(['ConductanceSets/Particle' num2str(ID) '.dat'],'data','-ascii')
numCPUs = feature('numCores'); % Get number of CPUs available
pc_storage_dir = fullfile('pc_temp_storage',getenv('SLURM_JOB_ID'));    % assign JobStorageLocation based on id of SLURM job that called this function
mkdir(pc_storage_dir);
pc = parcluster('local');
pc.JobStorageLocation = pc_storage_dir;
fprintf('Number of CPUs requested = %g\n',numCPUs);
poolobj = parpool(pc,numCPUs-1); % initialize pool of workers, i.e. CPUs in this node to assign tasks, leave 1 CPU to handle the overhead

parfor q=1:4
    BatchCV(D(q),modelType,ID,dt,1000,37,data(end));
end
parfor q=1:4
    BatchCV(D(q),modelType,ID,dt,1000,24,data(end));
end
BatchSDNew2(ID,modelType,PW,D,dt,tstop,data(end));
parfor q=1:4
    BatchCV(D(q),modelType,ID,dt,1000,24,data(end));
%     BatchThresh(10,0,D(q),modelType,ID,0.01,20,data(end),24,1,-1)
end

%% Parallelize Recovery Cycle - edit to only do 2 threshold detections and 2 if fires
% totsims=length(D)*length(ID)*(length(IPI));
% parfor i=1:totsims
%    indD=mod(i-1,length(D))+1;
%    j=floor((i-1)/length(D))+1;
%    indIPI=floor((j-1)/length(ID))+1;
%    BatchThresh(0.1,IPI(indIPI),D(indD),modelType,ID,dt,IPI(indIPI)+tstop,data(end),33,-1,20);
% end

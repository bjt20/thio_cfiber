%% Load in Base Data
function Runner3_MinNeeded(ID)
addpath('./PSOFunctions');
addpath('..');
cd(['Elect'])
latestFile=getlatestfile('.');
j=1;
while ~strcmp(['Swarm' num2str(j) '.mat'],latestFile) & ~strcmp(['Outcomes' num2str(j) '.mat'],latestFile)
    j=j+1;
end
load(['Swarm' num2str(j) '.mat'])
val=max(sum(bp));
cd('..')

ID2=[find(sum(bp)==val)];

modelType = 5;%4;
% list of pulse widths in ms
PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 2 10];
% list of diameters in um
D = 1;%[0.5 1 1.5 2];
% list of IPI in ms
IPI = 1:2:250;%;[13 23 51 101 250];
dt=0.01;
tstop=20;
data=load(['ConductanceSets/Particle' num2str(ID2) '.dat']);

% sortedCond=sort(data(1:14));
sortedCond=[3     4     5     6     7     8    10    11    12    13    14     1     9     2];
for i=1:ID
    data(sortedCond(i))=0;
end

save(['ConductanceSets/Particle' num2str(ID*10000+1) '.dat'],'data','-ascii')

numCPUs = feature('numCores'); % Get number of CPUs available
pc_storage_dir = fullfile('pc_temp_storage',getenv('SLURM_JOB_ID'));    % assign JobStorageLocation based on id of SLURM job that called this function
mkdir(pc_storage_dir);
pc = parcluster('local');
pc.JobStorageLocation = pc_storage_dir;
fprintf('Number of CPUs requested = %g\n',numCPUs);
poolobj = parpool(pc,numCPUs-1); % initialize pool of workers, i.e. CPUs in this node to assign tasks, leave 1 CPU to handle the overhead
%% CV
% parfor q=1:4
%     BatchCV(D(q),modelType,ID*10000+1,dt,1000,37,data(end));
% end
% %% APD
% data=load(['ConductanceSets/Particle' num2str(ID2) '.dat']);
% sortedCond=[13    14     7     8    12     6     4    11     3     5     1    10     9     2];
% for i=1:ID
%     data(sortedCond(i))=0;
% end
% 
% save(['ConductanceSets/Particle' num2str(ID*10000+2) '.dat'],'data','-ascii')
% 
% parfor q=1:4
%     BatchCV(D(q),modelType,ID*10000+2,dt,1000,24,data(end));
% end
% %% SD
% data=load(['ConductanceSets/Particle' num2str(ID2) '.dat']);
% sortedCond=[4     6     7     8    11    13    14     3     5    12    10     1     9     2];
% for i=1:ID
%     data(sortedCond(i))=0;
% end
% 
% save(['ConductanceSets/Particle' num2str(ID*10000+3) '.dat'],'data','-ascii')
% 
% BatchSDNew2(ID*10000+3,modelType,PW,D,dt,tstop,data(end));
% %% Thresh
% data=load(['ConductanceSets/Particle' num2str(ID2) '.dat']);
% sortedCond=[6     7     8    13    14     4     5    11     3    12    10     1     2     9];
% for i=1:ID
%     data(sortedCond(i))=0;
% end
% 
% save(['ConductanceSets/Particle' num2str(ID*10000+4) '.dat'],'data','-ascii')
% parfor q=1:4
%     BatchThresh(10,0,D(q),modelType,ID*10000+4,0.01,20,data(end),24,1,-1)
% end

%% Parallelize Recovery Cycle - edit to only do 2 threshold detections and 2 if fires
data=load(['ConductanceSets/Particle' num2str(ID2) '.dat']);
sortedCond=[4     7    11    13     8    14     6    10     3     5     1     9     2    12];
for i=1:ID
    data(sortedCond(i))=0;
end

save(['ConductanceSets/Particle' num2str(ID*10000+5) '.dat'],'data','-ascii')

BatchSDNew2(ID*10000+5,modelType,PW,D,dt,tstop,data(end));

totsims=length(D)*length(ID*10000)*(length(IPI));
parfor i=1:totsims
   indD=mod(i-1,length(D))+1;
   j=floor((i-1)/length(D))+1;
   indIPI=floor((j-1)/length(ID*10000))+1;
   BatchThresh(0.1,IPI(indIPI),D(indD),modelType,ID*10000+5,dt,IPI(indIPI)+tstop,data(end),33,-1,20);
end

function knowledge = loadKnowledge(dir_name,version)

global idealOutcomes

kfile = [dir_name '/knowledge.mat'];
if ~isfile(kfile)
    knowledge.testPatterns = [];
    knowledge.conductanceSets = [];
    knowledge.patternOutcomes = [];
    knowledge.patternFitnesses = [];
    knowledge.patternScores = [];
    knowledge.Sensitivity = [];
    knowledge.analogOutcomes = [];
    knowledge.analogScores = [];
    knowledge.FunctionMatrix = {};
    knowledge.ideals = idealOutcomes;

    save(kfile,'knowledge');
else
    load(kfile);
end

end

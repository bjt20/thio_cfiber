function updateKnowledge2(knowledge,dir_name)

knowledge.Sensitivity = zeros(2,length(knowledge.testPatterns(1,:)),length(knowledge.analogOutcomes(1,:)));

tempSTD = std(knowledge.testPatterns);
[~,bestScoreInd] = min(knowledge.analogScores);

for i = 1:length(knowledge.analogOutcomes(1,:)) %loop through outcomes
    
    tempCorr = zeros(2,length(knowledge.testPatterns(1,:)));
    
    for j = 1:length(knowledge.testPatterns(1,:)) %loop through particle locations (conductances)
        
        %0.5->1.5 sd
        tempInds = (knowledge.testPatterns(:,j)<(knowledge.testPatterns(bestScoreInd,j)+1.5*tempSTD(j)))&(knowledge.testPatterns(:,j)>(knowledge.testPatterns(bestScoreInd,j)+0.5*tempSTD(j)));
        tempMiddle = knowledge.analogOutcomes(tempInds,i);
        try %possible empty result
            tempCorr(1,j) = mean(tempMiddle(tempMiddle<1e6));
        catch
            tempCorr(1,j) = 1e9;
        end
        
        %-1.5->-0.5 sd
        tempInds = (knowledge.testPatterns(:,j)<(knowledge.testPatterns(bestScoreInd,j)-0.5*tempSTD(j)))&(knowledge.testPatterns(:,j)>(knowledge.testPatterns(bestScoreInd,j)-1.5*tempSTD(j)));
        tempMiddle = knowledge.analogOutcomes(tempInds,i);
        try %possible empty result
            tempCorr(2,j) = mean(tempMiddle(tempMiddle<1e6));
        catch
            tempCorr(2,j) = 1e9;
        end
        
        %within 0.5 sd
        tempInds = (knowledge.testPatterns(:,j)<(knowledge.testPatterns(bestScoreInd,j)+0.5*tempSTD(j)))&(knowledge.testPatterns(:,j)>(knowledge.testPatterns(bestScoreInd,j)-0.5*tempSTD(j)));
        tempMiddle = knowledge.analogOutcomes(tempInds,i);
        try %possible empty result
            tempMiddle = mean(tempMiddle(tempMiddle<1e6));
        catch
            tempMiddle = 0;
        end
        
        tempCorr(1,j) = tempCorr(1,j)-tempMiddle; %outcome change by decrease value
        tempCorr(2,j) = tempCorr(2,j)-tempMiddle; %outcome change by increase value
        tempCorr(tempCorr(:,j)>1e6 | isnan(tempCorr(:,j)),j) = 0; %if we don't know, we pretend there is no effect

    end
    
    try %possible divide by zero
        %knowledge.Sensitivity(:,:,i) = tempCorr./max(max(abs(tempCorr)));
         knowledge.Sensitivity(:,:,i) = tempCorr;
    catch
        knowledge.Sensitivity(:,:,i) = ones(size(knowledge.Sensitivity(:,:,i)));
    end

end

knowledge = updateFunctionMatrix(knowledge);

save([dir_name '/knowledge.mat'],'knowledge');
disp(['knowledge base updated size: ' num2str(length(knowledge.patternScores))])

end

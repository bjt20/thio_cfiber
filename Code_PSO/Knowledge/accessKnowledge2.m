function vexp = accessKnowledge2(knowledge,dimensionValues)

    anOut = dimensionValues(1).analog; %a particular particles flaws
    
    anOut(anOut>1e6) = 0; %don't update velocities using bad values
    
    unalteredValues = 1:length(knowledge.conductanceSets(1,:));
    
    vexp = zeros(1,length(knowledge.conductanceSets(1,:)));
    
    strengthFactor = 1;
    
    Sensitivity = accessFunctionMatrix(knowledge,dimensionValues);
    
    for i = 1:length(knowledge.conductanceSets(1,:))-1
        
        [~,outInd] = max(abs(anOut)); %largest remaining problem
        
        tempSens = squeeze(Sensitivity(:,:,outInd));

        if (anOut(outInd) > 0) %reduce Outcome value
            [row,col] = find(tempSens==min(min(tempSens(:,unalteredValues))),1);
        else %increase Outcome value
            [row,col] = find(tempSens==max(max(tempSens(:,unalteredValues))),1);
        end
        
        if (row>1) %increase strength of channel
            vexp(col) = strengthFactor;
        else %decrease strength of channel
            vexp(col) = -1*strengthFactor;
        end
        
        unalteredValues = unalteredValues(unalteredValues~=col);

        anOut = anOut + strengthFactor.*reshape(squeeze(Sensitivity(row,col,:)),size(anOut));
        
        strengthFactor = strengthFactor*0.8;
        
    end
    
    vexp = vexp./norm(vexp);

end

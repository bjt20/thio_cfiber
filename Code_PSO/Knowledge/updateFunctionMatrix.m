function knowledge = updateFunctionMatrix(knowledge)

knowledge.FunctionMatrix = cell(length(knowledge.testPatterns(1,:)),length(knowledge.analogOutcomes(1,:)));


for k = 1:length(knowledge.analogOutcomes(1,:))
    %Conductances to conductances
    cSets = [log10(knowledge.conductanceSets(:,1:16)) knowledge.conductanceSets(:,17)];
    outcomeValues = knowledge.analogOutcomes(:,k);
    
    
    cSets(isinf(cSets)) = -6;
    cSets(outcomeValues>4.5|outcomeValues<-4.5,:) = [];
    outcomeValues(outcomeValues>4.5|outcomeValues<-4.5) = [];
    [~,A] = rmoutliers(outcomeValues);
    cSets(A,:) = [];
    outcomeValues(A,:) = [];
    x0 = [2 5 2 -2];
    upper = [10 10 6 5];
    lower = [-10 -10 -3 -5];
    fitfun = fittype( @(a,b,c,d,x) a./(1+exp(b*(x+c)))+d );
    for i = 1:17
        try
        if (i==17)
            upper = [10 10 -50 5];
            lower = [-10 -10 -100 -5];
        end
        tempC = cSets(:,i);
        [~,A] = rmoutliers(tempC);
        test2 = outcomeValues;
        tempC(A) = [];
        test2(A) = [];
        F = fit(tempC,test2,fitfun,'StartPoint',x0,'diffMinChange',1e-2,'Upper',upper,'Lower',lower);
        knowledge.FunctionMatrix{i,k} = F;
        catch
            knowledge.FunctionMatrix{i,k} = {};
        end
    end
end

end

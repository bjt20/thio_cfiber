function Sensitivity = accessFunctionMatrix(knowledge,dimensionValues)

%rebuild conductance sets from location data
cSet = [];
numParts = length(knowledge.testPatterns(1,:));
for k=1:numParts
    if (k<(numParts-2))
        cSet(k) = (10.^(dimensionValues(end-1).loc)).*(10.^(dimensionValues(k).loc)./sum(10.^[dimensionValues(1:(end-3)).loc]));
    elseif(k<numParts)
        cSet(k) = 10.^(dimensionValues(k).loc); %Ra/total conductance
    else
        cSet(k) = dimensionValues(k).loc; %v_rest
    end
end

% for k=1:numParts
%     if (k<(numParts-2))
%         cSet(k) = (10.^(dimensionValues(end-1))).*(10.^(dimensionValues(k))./sum(10.^[dimensionValues(1:(end-3))]));
%     elseif(k<numParts)
%         cSet(k) = 10.^(dimensionValues(k)); %Ra/total conductance
%     else
%         cSet(k) = dimensionValues(k); %v_rest
%     end
% end

%into form used by functions
cSet(1:16) = log10(cSet(1,1:16));

%build new sensitivity matrix specific to particle location
Sensitivity = zeros(2,length(knowledge.testPatterns(1,:)),length(knowledge.analogOutcomes(1,:)));

for i = 1:length(knowledge.analogOutcomes(1,:)) %loop through outcomes
    for j = 1:length(knowledge.testPatterns(1,:))
        try
            F = knowledge.FunctionMatrix{j,i};
            Sensitivity(1,j,i) = F(cSet(i)-abs(cSet(i)*0.05))-F(cSet(i)); %decrease
            Sensitivity(2,j,i) = F(cSet(i)+abs(cSet(i)*0.05))-F(cSet(i)); %increase
        catch
            Sensitivity(1,j,i) = 0;
            Sensitivity(2,j,i) = 0;
        end
    end
end

end

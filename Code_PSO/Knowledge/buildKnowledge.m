function knowledge = buildKnowledge(knowledge,k,swarm,VTrace,CV,APW,Chron,Refract,SuperPeak,Transition,SubPeak,D)

newPattern = zeros(1,length(swarm(:,k)));
for i = 1:length(newPattern)
    newPattern(i) = swarm(i,k).loc;
end

conds = importdata(['ConductanceSets/Particle' num2str(k) '.dat']);

knowledge.testPatterns(end+1,:) = newPattern;
knowledge.conductanceSets(end+1,:) = conds';
knowledge.patternFitnesses(end+1,:) = swarm(1,k).fitness;
knowledge.patternScores(end+1) = sum(swarm(1,k).fitness);
knowledge.patternOutcomes(end+1,:) = zeros(1,length(CV)*(length(knowledge.ideals)+1));
knowledge.analogOutcomes(end+1,:) = zeros(1,length(knowledge.ideals));

for i = 1:length(CV)

if (Chron(i)==-1)
    Chron(i) = -1;
else
    Chron(i) = Chron(i)/1000; %convert us to ms
end

knowledge.patternOutcomes(end,i) = min(VTrace(:,i)); %mV
knowledge.patternOutcomes(end,i+length(CV)) = CV(i); %m/s
knowledge.patternOutcomes(end,i+length(CV)*2) = Chron(i); %ms
knowledge.patternOutcomes(end,i+length(CV)*3) = SuperPeak(i); %xThreshold
knowledge.patternOutcomes(end,i+length(CV)*4) = APW(i); %ms
knowledge.patternOutcomes(end,i+length(CV)*8) = Refract(i); %xThreshold
knowledge.patternOutcomes(end,i+length(CV)*5) = SubPeak(i); %xThreshold
knowledge.patternOutcomes(end,[i+length(CV)*6 i+length(CV)*7]) = Transition(:,i); %xThreshold
knowledge.patternOutcomes(end,i+length(CV)*9) = VTrace(end,i); %mV

%Recursive addition of knowledge.analogOutcomes accounts for multiple diameters

if (min(VTrace(:,i))>knowledge.ideals(1))
    knowledge.analogOutcomes(end,1) = 0 + knowledge.analogOutcomes(end,1);
elseif (min(VTrace(:,i))==-1) %crashed score
    knowledge.analogOutcomes(end,1) = knowledge.analogOutcomes(end,1) + 1e9;
else
    %.333/mV below ideal minimum Vm
    knowledge.analogOutcomes(end,1) = knowledge.analogOutcomes(end,1) + (min(VTrace(:,i)) - knowledge.ideals(1))./20; 
end

if (CV(i)<=0) %crashed score
    knowledge.analogOutcomes(end,2) = knowledge.analogOutcomes(end,2) + 1e9;
else
    %1/(m/s) aways from ideal velocity
    knowledge.analogOutcomes(end,2) = knowledge.analogOutcomes(end,2) + (CV(i) - knowledge.ideals(2));
end

if (APW(i)<=0) %crashed score
    knowledge.analogOutcomes(end,5) = knowledge.analogOutcomes(end,5) + 1e9;
else
    %1/ms away from ideal APW
    knowledge.analogOutcomes(end,5) = knowledge.analogOutcomes(end,5) + (APW(i) - knowledge.ideals(5))/2;
end

if (Chron(i)<=0) %crashed score
    knowledge.analogOutcomes(end,3) = knowledge.analogOutcomes(end,3) + 1e9;
else
    %1/s off of ideal chronaxie
    knowledge.analogOutcomes(end,3) = knowledge.analogOutcomes(end,3) + (Chron(i) - knowledge.ideals(3));
end

if (SuperPeak(i)<=0) %crashed score
    knowledge.analogOutcomes(end,4) = knowledge.analogOutcomes(end,4) + 1e9;
else
    %.1/ms off of ideal reduction
    knowledge.analogOutcomes(end,4) = knowledge.analogOutcomes(end,4) + (log(SuperPeak(i)) - log(knowledge.ideals(4)));
end

if (Refract(i)<=0) %crashed score
    knowledge.analogOutcomes(end,8) = knowledge.analogOutcomes(end,8) + 1e9;
else
    %.05/ms off of ideal
    if (Refract(i)>1) %passes test
        knowledge.analogOutcomes(end,8) = knowledge.analogOutcomes(end,8) + 0;
    else
        knowledge.analogOutcomes(end,8) = knowledge.analogOutcomes(end,8) + (log(Refract(i)) - log(knowledge.ideals(8)));
    end
end

if (SubPeak(i)<0) %crashed score
    knowledge.analogOutcomes(end,6) = knowledge.analogOutcomes(end,6) + 1e9;
else
    %1/spike off of ideal increase
    knowledge.analogOutcomes(end,6) = knowledge.analogOutcomes(end,6) + (log(SubPeak(i)) - log(knowledge.ideals(6)));
end

if (sum(Transition(:,i)<0)>0) %crashed score
    knowledge.analogOutcomes(end,7) = knowledge.analogOutcomes(end,7) + 1e9;
else
    %1/spike off of ideal spike number
    knowledge.analogOutcomes(end,7) = knowledge.analogOutcomes(end,7) + (log(mean(Transition(:,i))) + 10*(Transition(1,i)>Transition(2,i)) - log(knowledge.ideals(7)));
end


if (VTrace(end,i)<knowledge.ideals(end))
    knowledge.analogOutcomes(end,9) = knowledge.analogOutcomes(end,9) + 0;
elseif (VTrace(end,i)==-1) %crashed score
    knowledge.analogOutcomes(end,9) = knowledge.analogOutcomes(end,9) + 1e9;
else
    %.1/mV over max end V
    knowledge.analogOutcomes(end,9) = knowledge.analogOutcomes(end,9) + (VTrace(end,i) - knowledge.ideals(9))./20;
end

end

%Added a penalty for scores that fail in binary land, 10x for a zero, 1x for a 1
knowledge.analogOutcomes(end,2:end-1) = knowledge.analogOutcomes(end,2:end-1).*(1+9.*(1-knowledge.patternFitnesses(end,:)));

%absolute value, multiply by 100 (just for easy reading), and sum results
knowledge.analogScores(end+1) = sum(100*abs(knowledge.analogOutcomes(end,:)));

end

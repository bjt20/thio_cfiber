function [swarm_elect,topDog] = elect(swarm,iteration)

global numParts numDim

%% Find all nondominated soutions and put their indicies into leaders
swarm_elect = swarm;

swarmind=1:numDim;
swarmMetric = [];
for i=1:numDim
    swarmMetric=[swarmMetric [swarm_elect(1,i).analog(2:8)']];% ignore 1 and 9 when making nondominated
end

potential=swarmMetric;
save(['Elect/Outcomes' num2str(iteration) '.mat'],'potential')

swarmMetric = abs(swarmMetric);

potential = sum(swarmMetric);% sum the output magnitudes into single scores
best=find(min(potential)==potential); % find a non-dominated solution
topDog=potential(best(1));
leaders=swarmind(best(1)); % put its index into the leaders vector
swarmMetricTemp=swarmMetric;
col=1;
while 0<length(swarmMetricTemp(1,:))
    temp=swarmMetricTemp;
    % check dominated
    dom=temp<swarmMetric(:,leaders(col));
    temp(:,sum(dom)==0)=[];
    swarmind(sum(dom)==0)=[];


    if ~isempty(temp)
        col=col+1;
        potential = sum(temp);% sum the binary outputs for each set of conductances
        best=find(min(potential)==potential); % find a non-dominated solution
        leaders=[leaders swarmind(best(1))]; % put its index into the leaders vector
    end
    swarmMetricTemp=temp;
    if (col>length(swarmMetric(1,:)))
        quit;
    end
end
%% Add the analog scoring leader into the leaders vector
% potential = [];
% for i=1:numDim
%     potential=[potential [swarm_elect(1,i).analog]];% ignore 1 and 9 when making nondominated
% end
% potential = sum(potential);
% best=find(min(potential)==potential);
% leaders=[leaders swarmMetric(end,best(1))];

%% Establish Connections setup so that leaders are passed onto next gen and swarm is grouped into n parts where n is number of leaders
%for nondominated pass onto next gen
ngroups=length(leaders);
nondomLoc=[];
for i=1:ngroups
    particleLoc=[];
    for k=1:numParts
        swarm_elect(k,leaders(i)).lead1 = swarm_elect(k,leaders(i)).loc;
        swarm_elect(k,leaders(i)).pBest_loc = swarm_elect(k,leaders(i)).loc;
        particleLoc=[particleLoc;swarm_elect(k,leaders(i)).loc];
    end
    nondomLoc=[nondomLoc,particleLoc];
end
save(['NonDomSolutions/NonDom' num2str(iteration) '.mat'],'nondomLoc')
% leaders=leaders(1);
groups=1:numDim;
groups(leaders)=[];
groups_rand = groups;%do not randomize the neighborhoods per generation
% groups_rand = groups(randperm(length(groups)));%make the pie

%for neighbors
for i = 1:length(groups_rand)
    for k=1:numParts
        whichLeader=mod(i,ngroups)+1;%slice up the pie
        swarm_elect(k,groups_rand(i)).lead1 = swarm_elect(k,leaders(whichLeader)).loc; %pBest_loc from loc
        swarm_elect(k,groups_rand(i)).pBest_loc = swarm_elect(k,leaders(whichLeader)).loc;
    end
end

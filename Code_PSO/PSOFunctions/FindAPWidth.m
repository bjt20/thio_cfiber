function APWidth = FindAPWidth(VoltageTrace,TimeTrace)
dvdt=diff(VoltageTrace);
allpos=find((VoltageTrace-VoltageTrace(1))>(0.1*max(VoltageTrace-VoltageTrace(1))));

if length(allpos)>0
    startTime=allpos(1);
    startV=VoltageTrace(startTime);
    [intersectx,intersecty]=intersections(TimeTrace,VoltageTrace,TimeTrace,linspace(startV,startV,length(TimeTrace)));
    
    
%     falling=diff(endTime);
    if length(intersectx)>1
        endTime=intersectx(2);
%         loc=find(abs(falling)>5);
        APWidth=endTime-TimeTrace(startTime);
    else
        APWidth=0;
    end
else 
    APWidth=0;
end
% function APWidth = FindAPWidth(VoltageTrace,TimeTrace)
% if sum(isnan(VoltageTrace))==0
% %     threshold=(max(VoltageTrace)+60)/2-60;%half way up AP
%     threshold=(max(VoltageTrace)+60)/10-60;%10 up
%     dvdt=diff(VoltageTrace);
%     allpos=find(dvdt>(0.01*max(dvdt)));
%     disp('1')
%     if length(allpos)>0
%         disp('2')
% %         startTime=threshold;
%         startV=threshold;%VoltageTrace(startTime);
%         [intersectx,intersecty]=intersections(TimeTrace,VoltageTrace,TimeTrace,linspace(startV,startV,length(TimeTrace)));
%         
%         if length(intersectx)>1
%             endTime=intersectx(2);
%             APWidth=endTime-intersectx(1);
%         else
%             APWidth=0;
%         end
%     else
%         APWidth=0;
%     end
% else
%     APWidth=0;
% end

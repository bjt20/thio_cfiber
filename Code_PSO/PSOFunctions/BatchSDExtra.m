function BatchSDExtra(PW,Diam,modelType,ParticleID,dt,tstop,v_init,celsius)
%% PW to run
% modelType 1:Sundt 2:Tigerholm 3:Rattay 4:Schild
len=2500;%Stable[um]
segdensity=50/6;%segment length [um]
initialdel=0; % pulse delay
sigma=0.2;% conductivity of medium in S/m
%% Togglable
dura=PW; % duration of pulse [ms]
type =modelType; % fiber type 1:Sundt 2:Tigerholm 3:Rattay
dist=0.00015;% distance from fiber in meters
% freq=10000;
if PW<1000
    tstop=PW+tstop;
end
id=ParticleID;
for k=id
    for i=1:length(Diam)
        D=Diam(i);
        ParticleID=k;
        call_neuron_MATLAB('SDExtra.hoc',ParticleID,segdensity,dt,tstop,len,D,type,dura,initialdel,sigma,dist,v_init,celsius);
    end
end

function [swarm,topDog] = elect2(swarm,iteration)
% Find top performing particles and "elect" them into leadership positions

global numParts numDim numOutcomes

%% Gather and Save Input Swarm and Fitness Data to file
swarmMetric = [];
for i=1:numDim
    swarmMetric=[swarmMetric [swarm(1,i).analog(1:numOutcomes+2)']];% include 1 and 9 for breaking
end
bp=[];
for i=1:numDim
    bp=[bp [swarm(1,i).fitness]];
end
p = swarmMetric;
save(['Elect/Swarm' num2str(iteration) '.mat'],'swarm','p','bp')


%% Add Old Leaders to Swarm
swarm_elect = swarm; %Change Var Name to Protect against Loads

if (iteration>1) %add old leaders to swarm and scores
    
    %load and find old leaders
    load(['Elect/Outcomes' num2str(iteration-1) '.mat'])
    oldleaders = findLeaders(potential); 
    % non-dominated leaders of top score and each criterion
    
    %add leaders to end of swarm
    for i = 1:length(oldleaders)
        swarmMetric(:,end+1) = potential(:,oldleaders(i));
        swarm_elect(:,end+1) = swarm(:,oldleaders(i));
    end

end %end old leader replacement

%% Find Leaders of Combined Swarm
leaders = findLeaders(swarmMetric);
% non-dominated leaders of top score and each criterion
% if old leaders did not redominate, old leaders will be dropped

%% Establish Connections setup so that leaders are passed onto next gen and swarm is grouped into n parts where n is number of leaders
nonleaders = 1:length(swarm_elect(1,:));
nonleaders(leaders) = [];
%Set non-leader particle leaders
for i = 1:numDim-length(leaders)
    swarm(:,i) = swarm_elect(:,nonleaders(i)); %Move particles to beginning of swarm
    for k=1:numParts %set leader details
        whichLeader=mod(i,length(leaders))+1;%slice up the pie
        swarm(k,i).lead1 = swarm_elect(k,leaders(whichLeader)).loc; %pBest_loc from loc
        swarm(k,i).pBest_loc = swarm_elect(k,leaders(whichLeader)).loc;
    end
end

%Set leaders to global leader
for i=numDim-length(leaders)+1:numDim
    swarm(:,i) = swarm_elect(:,leaders(i-numDim+length(leaders))); %Move particles to end of swarm
    for k=1:numParts
        swarm(k,i).lead1 = swarm_elect(k,leaders(1)).loc; %set to global leader
        swarm(k,i).pBest_loc = swarm_elect(k,leaders(1)).loc;
    end
end

%% Gather and Save Output Swarm and Fitness Data to file
potential = [];
for i=1:numDim
    potential=[potential [swarm(1,i).analog(1:numOutcomes+2)']];% include 1 and 9 for breaking
end
binarypotential=[];
for i=1:numDim
    binarypotential=[binarypotential [swarm(1,i).fitness]];
end
save(['Elect/Outcomes' num2str(iteration) '.mat'],'swarm','potential','binarypotential')

topDog = max(sum(binarypotential(:,:)));

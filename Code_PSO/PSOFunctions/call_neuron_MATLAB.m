function call_neuron_MATLAB(file,ParticleID,varargin)

options = '';
if nargin > 2 %Build options using input variables
    for i = 1:length(varargin)
        if isnumeric(varargin{i})
            options = [options sprintf(' -c %s=%d',inputname(i+2),varargin{i})];
        else
            options = [options sprintf(' -c "{sprint(%s,\\"%s\\")}"',inputname(i+1),varargin{i})];
        end        
    end
end
options = [options sprintf(' -c ParticleID=%d',ParticleID)];
options = [options sprintf(' -c "load_file(\\"%s\\")"',file)];
options = [options ' -c "run_nrn_script()" -c "quit()"'];

if ispc
    system(['D:/nrn/bin/nrniv.exe -nobanner ' file options]);
elseif isunix
    disp(['./special -nobanner blank.hoc' options])
%     if strcmp(file,'CV.hoc')
%         [status,cmdout] = system(['ulimit -t 200; ./special -nobanner blank.hoc' options]);
%     elseif strcmp(file,'SDExtra.hoc')
%         [status,cmdout] = system(['ulimit -t 600; ./special -nobanner blank.hoc' options]);
%     elseif strcmp(file,'Thresh.hoc')
%         [status,cmdout] = system(['ulimit -t 300; ./special -nobanner blank.hoc' options]);    
%     else
        [status,cmdout] = system(['./special -nobanner blank.hoc' options]);
%     end
else
    error('Neither PC nor Unix');
end


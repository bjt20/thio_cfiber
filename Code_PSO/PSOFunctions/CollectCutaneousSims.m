function swarm = CollectCutaneousSims(swarm,ParticleIDs,knowledge)
% RunAutonomicSims is used to gather the data for CV, SD, and RC analysis

global D PW IPI modelType numOutcomes numParts

for m = 1:length(ParticleIDs)
    k=ParticleIDs(m);
    CV=zeros(length(D),1);
    VTrace = [];
    VTrace2 = [];
    SD=zeros(length(PW),length(D));
    TH=zeros(length(D),1);
    SuperPeak=zeros(length(D),1);
    SubPeak=zeros(length(D),1);

    %retrieving CV,SD, and RC results
    for diam=1:length(D)
        %CV
        name=['CV/c_fiber_CV_37_C_' num2str(D(diam)*1000) '_D_' num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            data=load(name);
            if isempty(data)
                CV(diam)=-1;
            else
                CV(diam)=data;
                CV(CV==0)=-1;
            end
        else
            CV(diam)=-1;
        end
        %End CV
        
        % Voltage Trace
        name=['VoltageTrace/c_fiber_37_C_' num2str(D(diam)*1000) '_D_'...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            voltage=load(name);
            if ~isempty(VTrace)
                if length(voltage)==length(VTrace)
                    if sum(isnan(voltage))==0
                        VTrace=[VTrace voltage];
                    else
                        VTrace = [-1 -1];
                    end
                else
                    VTrace=[-1 -1];
                end
            else
                if diam==1
                    if sum(isnan(voltage))==0
                        VTrace=[VTrace voltage];
                    else
                        VTrace = [-1];
                    end
                else
                    VTrace = [-1 -1];
                end
            end
        else
            VTrace=[-1 -1];
        end
        %End VTrace
        
        % Voltage Trace
        name=['VoltageTrace/c_fiber_24_C_' num2str(D(diam)*1000) '_D_'...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            voltage2=load(name);
            if ~isempty(VTrace2)
                if length(voltage2)==length(VTrace2)
                    if sum(isnan(voltage2))==0
                        VTrace2=[VTrace2 voltage2];
                    else
                        VTrace2 = [-1 -1];
                    end
                else
                    VTrace2=[-1 -1];
                end
            else
                if diam==1
                    if sum(isnan(voltage2))==0
                        VTrace2=[VTrace2 voltage2];
                    else
                        VTrace2 = [-1];
                    end
                else
                    VTrace2 = [-1 -1];
                end
            end
        else
            VTrace2=[-1 -1];
        end
        %End VTrace2
        
        % Pull 10 ms current threshold
        name=['Thresh/c_fiber_Thresh_24_C_' num2str(D(diam)*1000)...
            '_D_' num2str(10000) '_Duration_' num2str(0) '_Delay_' num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            PP=load(name);
            if ~isempty(PP)
                TH(diam)=PP(1);
            else
                TH(diam)=-1;
            end
        else
            TH(diam)=-1;
        end
        
        % Strength duration
        for dur=1:length(PW)
            name=['SD/c_fiber_Thresh_33_C_' num2str(D(diam)*1000)...
                '_D_' num2str(PW(dur)*1000) '_Duration_'...
                num2str(modelType) '_Type_' num2str(k) '.dat'];
            if exist(name)
                SDTemp=load(name);
                if ~isempty(SDTemp)
                    SD(dur,diam)=SDTemp;
                else
                    SD(dur,diam)=-1;
                end
            else
                SD(dur,diam)=-1;
            end
        end

        % Threshold for Refractory and RC
        try 
            start=load(['Thresh/c_fiber_Thresh_33_C_' num2str(D(diam)*1000) '_D_' num2str(100)...
            '_Duration_' num2str(0) '_Delay_' num2str(modelType) '_Type_'...
            num2str(k) '.dat']);
        catch
            start = 0;
        end

        % Pair Pulse SuperPeak
        name=['Thresh/c_fiber_Thresh_33_C_' num2str(D(diam)*1000)...
            '_D_' num2str(100) '_Duration_' num2str(IPI(2)) '_Delay_' ...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            PP=load(name);
            if ~isempty(PP)&start~=0
                SuperPeak(diam)=PP(1)/start;
            else
                SuperPeak(diam)=-1;
            end
        else
            SuperPeak(diam)=-1;
        end

        % Subnormal peak
        name=['Thresh/c_fiber_Thresh_33_C_' num2str(D(diam)*1000)...
            '_D_' num2str(100) '_Duration_' num2str(IPI(end)) '_Delay_' ...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            Temp=load(name);
            if ~isempty(Temp)&start~=0
                SubPeak(diam)=Temp/start;
            else
                SubPeak(diam)=-1;
            end
        else
            SubPeak(diam)=-1;
        end

    end %end diameter loop
    
    % Fill in outcome vector for:
    % 1: Both CV between 0.5 and 1.5 m/s
    outcome=zeros(numOutcomes,1);
    outcome(1)=length(CV)==sum(CV>0.5 & CV<1.5);
    sizes=size(VTrace2);
    
    if (length(VTrace2(isnan(VTrace2)))>0)|(length(VTrace2)<2000)%|(sizes(end)<2)
        TTrace=[-1];
    else
        TTrace=linspace(0,20,length(VTrace2(:,1))-1);
    end
    
    for g=1:length(D)
        %% Check Chronaxie
        if sum(SD(:,g)==-1|SD(:,g)==0)==0
            chronaxie(g)=findChronaxie(PW(1:end).*1000,SD(1:end,g));
        else
            chronaxie(g)=-1;
        end

        %% Check APD
        if (length(VTrace2)-1)==length(TTrace) & length(VTrace2)>2000 %& length(VTrace2(1,:))==2 %
            APWidth(g)=FindAPWidth(VTrace2(1:end-1,g),TTrace);
        else
            APWidth(g)=-1;
        end
    end %end diameter loop
    
    APWidth(APWidth==0)=-1;
    
    outcome(2)=all(chronaxie>500 & chronaxie<1500); 
    outcome(3)=all(SuperPeak>0.9 & SuperPeak<1.1); %agrees with paper not above
    outcome(4)=all(APWidth>1.5 & APWidth<2.6);
    outcome(5)=all(SubPeak>1.1 & SubPeak<1.3);
    outcome(6)=all((TH>.015) & (TH<.15));

    %zeros out the fitness of particles which do not stably fire an AP
%     if (CV(1)==-1 | CV(2)==-1 | abs(VTrace2(end,1)-swarm(end,k).loc)>10 | abs(VTrace2(end,2)-swarm(end,k).loc)>10 | abs(VTrace(end,1)-swarm(end,k).loc)>10 | abs(VTrace(end,2)-swarm(end,k).loc)>10 | all(VTrace(end,:)<-40) | min(min(VTrace(:,:)))>-90)
    if (any(CV==-1) | all(VTrace2(end,:)>-40) | min(min(VTrace2(:,:)))<-90)
        outcome=zeros(numOutcomes,1);
    end

    %updating fitness
    for i = 1:numParts
        swarm(i,k).fitness = outcome;
    end

    knowledge = buildKnowledge2(knowledge,k,swarm,VTrace2,CV,APWidth,chronaxie,TH,SuperPeak,SubPeak,D);

    %updating analog from knowledge
    for i = 1:numParts
        swarm(i,k).analog = knowledge.analogOutcomes(end,:);
    end
end


end

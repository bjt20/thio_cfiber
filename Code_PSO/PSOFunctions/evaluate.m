function [swarm, convergence] = evaluate(swarm,iteration)

global numParts numDim numOutcomes version analogversion

%% Write particle values to conductance dat file
for i=1:numDim
    file = fopen(['ConductanceSets/Particle' num2str(i) '.dat'],'w');
    for k=1:numParts
        if (k<(numParts-2))
            fprintf(file,'%7.6f\n',(10.^(swarm(end-1,i).loc)).*(10.^(swarm(k,i).loc)./sum(10.^[swarm(1:(end-3),i).loc])));
        elseif(k<numParts)
            fprintf(file,'%7.6f\n',10.^(swarm(k,i).loc)); %total conductance
        else
            fprintf(file,'%7.6f\n',(swarm(k,i).loc)); %v_rest
        end
    end
    fclose(file);
end

%% Run Test Simulations
if (version == 1)
    ParticleIDs = RunAutonomicSims(swarm);
else
    ParticleIDs = RunCutaneousSims(swarm);
end

%% Retrieve Thresholds, Calculate Cost, Update Fitness
%Reset fitness
for k=1:numDim
    outcome=zeros(numOutcomes,1);
    for i = 1:numParts
        swarm(i,k).fitness = outcome;
        swarm(i,k).analog = ones(1,numOutcomes+2).*1e9;
    end
end

%Collect data for propagating particles and put it in swarm variable
knowledge = loadKnowledge([pwd '/Knowledge'],version);
if (version == 1)
    swarm = CollectAutonomicSims(swarm,ParticleIDs,knowledge);
else
    swarm = CollectCutaneousSims(swarm,ParticleIDs,knowledge);
end

%% Update Connectivity
if (analogversion == 1)
    [swarm,TopDog] = elect2(swarm,iteration);
else
    [swarm,TopDog] = elect(swarm,iteration);
end

%% Update Sensitivity and store Knowledge for Later
if (~isempty(knowledge.testPatterns))
    updateKnowledge2(knowledge,[pwd '/Knowledge']);
end

%% Determine Convergence - need to figure out how to do this
convergence = 0;
if (TopDog == numOutcomes)
    convergence = 1;
end


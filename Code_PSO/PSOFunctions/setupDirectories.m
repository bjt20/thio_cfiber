%% make directories
if exist('SD')~=7
    mkdir('SD')
end
if exist('CV')~=7
    mkdir('CV')
end
if exist('ConductanceSets')~=7
    mkdir('ConductanceSets')
end
if exist('Elect')~=7
    mkdir('Elect')
end
if exist('VoltageTrace')~=7
    mkdir('VoltageTrace')
end
if exist('Thresh')~=7
    mkdir('Thresh')
end
if exist('Knowledge')~=7
    mkdir('Knowledge')
end
if exist('pc_temp_storage')~=7
    mkdir('pc_temp_storage')
end
function BatchThresh(dura,del,D,type,ParticleID,dt,tstop,v_init,celsius,stimseg,chances)
% modelType 1:Sundt 2:Tigerholm 3:Rattay 4:Schild

len=2500;%Stable[um]
segdensity=50/6;%segment length [um]sacct
initialdel=0; % pulse delay
try
    if (del>0)
        start=load(['Thresh/c_fiber_Thresh_' num2str(celsius) '_C_' num2str(D*1000) '_D_' num2str(100)...
        '_Duration_' num2str(0) '_Delay_' num2str(type) '_Type_'...
        num2str(ParticleID) '.dat']);
    else
        start=0;
    end

    call_neuron_MATLAB('Thresh.hoc',ParticleID,segdensity,dt,tstop,len,D,type,dura,initialdel,v_init,celsius,stimseg,start,del,chances);
catch
    disp(['Particle ' num2str(ParticleID) ' Failed BatchThresh']);
end

function BatchCV(D,modelType,ID,dt,tstop,celsius,v_init)
%% stable
len=2500;%Stable[um]
segdensity=50/6;%segment length [um]
initialdel=0; % pulse delay
dura=0.1; % duration of pulse [ms]

%% Togglable
type = modelType; % fiber type 1:Sundt 2:Tigerholm 3:Rattay
ParticleID = ID;
call_neuron_MATLAB('CV.hoc',ParticleID,segdensity,dt,tstop,len,D,type,dura,initialdel,celsius,v_init);

end
function [swarm, convergence] = initialize()

global numOutcomes upper lower numDim numParts startingIteration

if (startingIteration==1)
    %% Initialize position and velocity
    for k = 1:numDim
        for i = 1:numParts
            swarm(i,k) = particle;
            swarm(i,k).loc = (upper(i)-lower(i)).*rand(1)+lower(i);
            swarm(i,k).vel = 0;
            swarm(i,k).pBest_loc = swarm(i,k).loc;
            swarm(i,k).fitness = zeros(numOutcomes,1);
            swarm(i,k).pBest = Inf;
            swarm(i,k).analog = 1e11.*ones(1,numOutcomes+2);
        end
    end

    %% Evaluate fitness and initialize fitness
    [swarm, convergence] = evaluate(swarm,startingIteration);
else
    %% Load Previous Swarm Data
    load(['Elect/Outcomes' num2str(startingIteration-1)]);
    [swarm, convergence] = evaluate(swarm,startingIteration);
    convergence = 0;
end

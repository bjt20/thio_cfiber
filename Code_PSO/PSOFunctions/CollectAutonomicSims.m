function swarm = CollectAutonomicSims(swarm,ParticleIDs,knowledge)
% RunAutonomicSims is used to gather the data for CV, SD, and RC analysis

global D PW IPI modelType numOutcomes numParts

for m = 1:length(ParticleIDs)
    k=ParticleIDs(m);
    CV=zeros(length(D),1);
    VTrace = [];
    SD=zeros(length(PW),length(D));
    Refract=zeros(length(D),1);
    SuperPeak=zeros(length(D),1);
    Transition=zeros(2,length(D));
    SubPeak=zeros(length(D),1);

    %retrieving CV,SD, and RC results
    for diam=1:length(D)
        %CV
        name=['CV/c_fiber_CV_37_C_' num2str(D(diam)*1000) '_D_' num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            data=load(name);
            if isempty(data)
                CV(diam)=-1;
            else
                CV(diam)=data;
                CV(CV==0)=-1;
            end
        else
            CV(diam)=-1;
        end
        %End CV

        % Voltage Trace
        name=['VoltageTrace/c_fiber_37_C_' num2str(D(diam)*1000) '_D_'...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            voltage=load(name);
            if ~isempty(VTrace)
                if length(voltage)==length(VTrace)
                    if sum(isnan(voltage))==0
                        VTrace=[VTrace voltage];
                    else
                        VTrace = [-1 -1];
                    end
                else
                    VTrace=[-1 -1];
                end
            else
                if diam==1
                    if sum(isnan(voltage))==0
                        VTrace=[VTrace voltage];
                    else
                        VTrace = [-1];
                    end
                else
                    VTrace = [-1 -1];
                end
            end
        else
            VTrace=[-1 -1];
        end
        %End VTrace

        % Strength duration
        for dur=1:length(PW)
            name=['SD/c_fiber_Thresh_37_C_' num2str(D(diam)*1000)...
                '_D_' num2str(PW(dur)*1000) '_Duration_'...
                num2str(modelType) '_Type_' num2str(k) '.dat'];
            if exist(name)
                SDTemp=load(name);
                if ~isempty(SDTemp)
                    SD(dur,diam)=SDTemp;
                else
                    SD(dur,diam)=-1;
                end
            else
                SD(dur,diam)=-1;
            end
        end

        % Threshold for Refractory and RC
        try
            start=load(['Thresh/c_fiber_Thresh_37_C_' num2str(D(diam)*1000) '_D_' num2str(100)...
            '_Duration_' num2str(0) '_Delay_' num2str(modelType) '_Type_'...
            num2str(k) '.dat']);
        catch
            start = 0;
        end

        % Pair Pulse Refractory
        name=['Thresh/c_fiber_Thresh_37_C_' num2str(D(diam)*1000)...
            '_D_' num2str(100) '_Duration_' num2str(IPI(1)) '_Delay_' ...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            PP=load(name);
            if ~isempty(PP)&start~=0
                Refract(diam)=PP(1)/start;
            else
                Refract(diam)=-1;
            end
        else
            Refract(diam)=-1;
        end

        % Pair Pulse SuperPeak
        name=['Thresh/c_fiber_Thresh_37_C_' num2str(D(diam)*1000)...
            '_D_' num2str(100) '_Duration_' num2str(IPI(2)) '_Delay_' ...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            PP=load(name);
            if ~isempty(PP)&start~=0
                SuperPeak(diam)=PP(1)/start;
            else
                SuperPeak(diam)=-1;
            end
        else
            SuperPeak(diam)=-1;
        end

        % Supernormal subnormal transition
        for trans=1:2
            name=['Thresh/c_fiber_Thresh_37_C_' num2str(D(diam)*1000)...
                '_D_' num2str(100) '_Duration_' num2str(IPI(trans+2)) '_Delay_' ...
                num2str(modelType) '_Type_' num2str(k) '.dat'];
            if exist(name)
                Temp=load(name);
                if ~isempty(Temp)&start~=0
                    Transition(trans,diam)=Temp/start;
                else
                    Transition(trans,diam)=-1;
                end
            else
                Transition(trans,diam)=-1;
            end
        end

        % Subnormal peak
        name=['Thresh/c_fiber_Thresh_37_C_' num2str(D(diam)*1000)...
            '_D_' num2str(100) '_Duration_' num2str(IPI(end)) '_Delay_' ...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            Temp=load(name);
            if ~isempty(Temp)&start~=0
                SubPeak(diam)=Temp/start;
            else
                SubPeak(diam)=-1;
            end
        else
            SubPeak(diam)=-1;
        end

    end

    % Fill in outcome vector for:
    % 1: Both CV between 0.45 and 1.76 m/s
    % 2: Chronaxie between 0.75 and 1.5 ms
    % 3: Max Decrease in Threshold recovery cycle between -50 and -20%
    % 4: AP width between 1.35 and 3 ms
    % 5: Magnitude of Subnormal period is >2%
    % 6: Transition from Supernormal to Subnormal between 50ms and 100ms IPI
    % 7: Transition from Refractory to supernormal after 11ms
    outcome=zeros(numOutcomes,1);
    outcome(1)=all(CV>0.45 & CV<1.76);
    sizes=size(VTrace);

    if (length(VTrace(isnan(VTrace)))>0)|(length(VTrace)<2000)%|(sizes(end)<2)
        TTrace=[-1];
    else
        TTrace=linspace(0,20,length(VTrace(:,1))-1);
    end

    for g=1:length(D)
        %% Check Chronaxie
        if sum(SD(:,g)==-1|SD(:,g)==0)==0
            chronaxie(g)=findChronaxie(PW(1:end).*1000,SD(1:end,g));
        else
            chronaxie(g)=-1;
        end

        %% Check APD
        if (length(VTrace)-1)==length(TTrace) & length(VTrace)>2000%& length(VTrace(1,:))==2 %& length(VTrace)>5000
            APWidth(g)=FindAPWidth(VTrace(1:end-1,g),TTrace);
        else
            APWidth(g)=-1;
        end
    end
    APWidth(APWidth==0)=-1;
    outcome(2)=all(chronaxie>750 & chronaxie<1500);
    outcome(3)=all(SuperPeak>0.7 & SuperPeak<0.9); %agrees with paper not above
    outcome(4)=all(APWidth>1.35 & APWidth<3);
    outcome(5)=all(SubPeak>1.02 & SubPeak<1.15);
    outcome(6)=all(Transition(1,:)<1 & Transition(2,:)>1);
    outcome(7)=all(Refract>1);

%     if (CV(1)==-1 | CV(2)==-1 | min(min(VTrace))<-90)%if does not propagate stably throw fiber away
    if (any(CV==-1) | min(min(VTrace))<-90)
        outcome=zeros(numOutcomes,1);
    end

    %updating fitness
    for i = 1:numParts
        swarm(i,k).fitness = outcome;
    end

    knowledge = buildKnowledge(knowledge,k,swarm,VTrace,CV,APWidth,chronaxie,Refract,SuperPeak,Transition,SubPeak,D);

    %updating analog from knowledge
    for i = 1:numParts
        swarm(i,k).analog = knowledge.analogOutcomes(end,:);
    end
end


end

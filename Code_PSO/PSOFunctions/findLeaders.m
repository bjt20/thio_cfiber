function leaders = findLeaders(swarmMetric)
    %finds indexes of leaders of group of analog scores
    %criteria X organisms
    leaders = [];

    % sum the output magnitudes into single scores
    swarmind=1:length(swarmMetric(1,:));
    potential = sum(abs(swarmMetric));
    
    best=find(min(potential)==potential); % find the best solution
    
    %save the value and location of top solution
    leaders=swarmind(best(1)); % put its index into the leaders vector
    
    tempind=swarmind;
    tempind(best(1)) = []; %do not allow overall leader to become secondary leader
    
    swarmMetricTemp=abs(swarmMetric(:,tempind));
    tempPotential=potential(tempind);
    
    %Cycle Through Main Criterion Scores (2-end-1)
    for i=2:(length(swarmMetric(:,1))-1)
        %find index of best criterion value
        best=find(swarmMetricTemp(i,:)==min(swarmMetricTemp(i,:)));
        
        %add index to leader list
        if length(best)>1
            topdog=find(tempPotential(best)==min(tempPotential(best)),1); %better score
            leaders=[leaders tempind(best(topdog))];
        else
            leaders=[leaders tempind(best)];
        end
    end
    
    leaders = unique(leaders,'stable');

end

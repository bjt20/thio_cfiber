classdef particle
    %particle object definition
    properties (Access = public)
        loc %current location
        vel %current velocity
        fitness %current fitness
        pBest %particle best fitness 
        pBest_loc %particle best location
        lead1 %influencing location 1
        analog
    end
    methods
        function obj = set.loc(obj,value)
            obj.loc = value;
        end
        function obj = set.vel(obj,value)
            obj.vel = value;
        end
        function obj = set.pBest_loc(obj,value)
            obj.pBest_loc = value;
        end
        function obj = set.pBest(obj,value)
            obj.pBest = value;
        end 
        function obj = set.lead1(obj,value)
            obj.lead1 = value;
        end
        function obj = set.fitness(obj,value)
            obj.fitness = value;
        end
        function obj = set.analog(obj,value)
            obj.analog = value;
        end
        %sort based on particle best fitness
        function [obj,idx]=sort(obj,varargin)
          [~,idx]=sort([obj.pBest],varargin{:}); obj=obj(idx);
        end
    end
end




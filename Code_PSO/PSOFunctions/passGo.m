function [swarm] = passGo(swarm,iteration)

global numParts numDim upper lower smartProp version startingIteration

%% Calculate New Velocities and Update
vAvg = zeros(1,numParts);
vIdeal=exp(-0.015*(iteration-startingIteration));
knowledge = loadKnowledge([pwd '/Knowledge'],version);
movementPattern = randperm(numDim);

% Intelligent Velocity Updating
for lamb = 1:floor(numDim*smartProp) %currently on
    k = movementPattern(lamb);
    try %smart
        vK = accessKnowledge2(knowledge,swarm(:,k))
        for i = 1:numParts
            exploration = vIdeal.*(2.*rand(1)-1); %Random Change
            exploitation = 0.13.*(rand(1).*(swarm(i,k).lead1-swarm(i,k).loc)); %Pull towards Leader
            inertia = vIdeal.*rand(1)*swarm(i,k).vel; %Inertial Factor
            if (isnan(inertia))
                inertia = 0;
            end
            %math results in reduction of up to 83% increase by up to 250%
            %assumes maximum values of -1:1 for vK(i) which isn't quite true
            %decays more slowly than exploration and inertia
            intelligence = vIdeal.^(0.75).*(vK(i)-0.5)/2; %Intelligence Factor (after log10 adjusts)
            swarm(i,k).vel = exploration+exploitation+inertia+intelligence;
            vAvg(i)=vAvg(i)+abs(swarm(i,k).vel);
        end
    catch %dumb
        for i = 1:numParts
            exploration = vIdeal.*(2.*rand(1)-1); %Random Change
            exploitation = 0.13.*(rand(1).*(swarm(i,k).lead1-swarm(i,k).loc)); %Pull towards Leader
            inertia = vIdeal.*rand(1)*swarm(i,k).vel; %Inertial Factor
            if (isnan(inertia))
                inertia = 0;
            end
            swarm(i,k).vel = exploration+exploitation+inertia;
            vAvg(i)=vAvg(i)+abs(swarm(i,k).vel);
        end
    end
end

% Dumb Velocity Updating
for lamb = floor(numDim*smartProp)+1:numDim
    k = movementPattern(lamb);
    for i = 1:numParts
        exploration = vIdeal.*(2.*rand(1)-1); %Random Change
        exploitation = 0.13.*(rand(1).*(swarm(i,k).lead1-swarm(i,k).loc)); %Pull towards Leader
        inertia = vIdeal.*rand(1)*swarm(i,k).vel; %Inertial Factor
        if (isnan(inertia))
            inertia = 0;
        end
        swarm(i,k).vel = exploration+exploitation+inertia;
        vAvg(i)=vAvg(i)+abs(swarm(i,k).vel);
    end
end


%% Dyanamic velocity weight updating
vAvg = vAvg./(numDim);  
for i=1:numParts
    if (vAvg(i) > 1.05*vIdeal) || (vAvg(i) < 0.95*vIdeal)
        for k = 1:numDim
            %adjust particle velocities to control velocity/dimension
            swarm(i,k).vel = swarm(i,k).vel.*(vIdeal./vAvg(i));
        end
    end
end


%% Location Updating: Adding Velocity
for k = 1:numDim
    for i = 1:numParts
        swarm(i,k).loc = swarm(i,k).loc + swarm(i,k).vel;
        %bounce off of upper and lower limits
        if swarm(i,k).loc<lower(i)
            swarm(i,k).loc=2.*lower(i)-swarm(i,k).loc;
            swarm(i,k).vel=-1.*swarm(i,k).vel;
        end
        if swarm(i,k).loc>upper(i)
            swarm(i,k).loc=2.*upper(i)-swarm(i,k).loc;
            swarm(i,k).vel=-1.*swarm(i,k).vel;
        end
    end
end


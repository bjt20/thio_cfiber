PW=[0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 2 10];
thresh=[];
for i=1:length(PW)
    d=load(['WhatChannelsDo\SD\NewRest50\c_fiber_Thresh_1000_D_' num2str(PW(i)*1000) '_Duration_4_Type_1003.dat']);
    thresh = [thresh d];
end
findChronaxie(PW(1:end).*1000,thresh(1:end))
plot(PW(1:end).*1000,thresh(1:end))

%%
cv=zeros(11,3);
for i=1:11
    for j=1:3
        d=load(['WhatChannelsDo\CV\c_fiber_CV_1000_D_4_Type_' num2str(i*100+j) '.dat']);
        cv(i,j)=d;
    end
end
%%

apw=zeros(11,3);
for i=1:11
    for j=1:3
        name=['WhatChannelsDo\VoltageTrace\new50\c_fiber_1000_D_4_Type_' num2str(i*100+j) '.dat'];
        if exist(name)
            d=load(name);
            t=linspace(0,50,length(d));
            apw(i,j)=FindAPWidth(d,t')
            plot(t,d)
            hold on
        end
    end
end
% plot(linspace(-3.25,50-3.25,length(d)),d./max(d)+1)

%%
% figure
IPI=[1:50 50:10:500];
d=[];
for i=1:length(IPI)
    d=[d load(['WhatChannelsDo\PP\NewRest50NoBalance\c_fiber_Thresh_1000_D_' num2str(IPI(i)) '_Delay_4_Type_501.dat'])];
end
plot(IPI,d./d(end))
hold on
axis([0 500 .7 1.035])
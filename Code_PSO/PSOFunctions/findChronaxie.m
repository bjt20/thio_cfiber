function chronaxie = findChronaxie(PW, thresh)
fo = fitoptions('Method','NonlinearLeastSquares',...
               'Lower',[0 0],...
               'Upper',[inf inf],...
               'StartPoint',[1 1000]);
ft = fittype('log10(rheobase.*(1+chronaxie/x))','options',fo);
% R is rheobase
% T is Chronaxie
% x is PW
[c,g] = fit(reshape(PW,[length(PW) 1]),log10(reshape(thresh,[length(thresh) 1])),ft);
chronaxie = c.chronaxie;
% rheo = thresh(end);
% chronaxie = spline(thresh,PW,rheo.*2);

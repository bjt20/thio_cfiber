function ParticleIDs = RunCutaneousSims(swarm)
% RunAutonomicSims is used to gather the data for CV, SD, and RC analysis

global numDim D modelType IPI

dt = 0.01;
tstop  = 20; 

%% Parallelize CV
parfor r=1:numDim
    for q=1:length(D)
        BatchCV(D(q),modelType,r,dt,20,37,swarm(end,r).loc);
        BatchCV(D(q),modelType,r,dt,20,24,swarm(end,r).loc);
    end
end

%% Check for broken particles and collect CV/VTrace data
ParticleIDs=[];
for k=1:numDim
    VTrace=[];
    VTrace2=[];
    for diam=1:length(D)
        %CV
        name=['CV/c_fiber_CV_37_C_' num2str(D(diam)*1000) '_D_' num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            data=load(name);
            if isempty(data)
                CV(diam)=-1;
            else
                CV(diam)=data;
                CV(CV==0)=-1;
            end
        else
            CV(diam)=-1;
        end
        %End CV
        
        % Voltage Trace
        name=['VoltageTrace/c_fiber_37_C_' num2str(D(diam)*1000) '_D_'...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            voltage=load(name);
            if ~isempty(VTrace)
                if length(voltage)==length(VTrace)
                    if sum(isnan(voltage))==0
                        VTrace=[VTrace voltage];
                    else
                        VTrace = [-1 -1];
                    end
                else
                    VTrace=[-1 -1];
                end
            else
                if diam==1
                    if sum(isnan(voltage))==0
                        VTrace=[VTrace voltage];
                    else
                        VTrace = [-1];
                    end
                else
                    VTrace = [-1 -1];
                end
            end
        else
            VTrace=[-1 -1];
        end
        %End VTrace
        
        % Voltage Trace
        name=['VoltageTrace/c_fiber_24_C_' num2str(D(diam)*1000) '_D_'...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            voltage2=load(name);
            if ~isempty(VTrace2)
                if length(voltage2)==length(VTrace2)
                    if sum(isnan(voltage2))==0
                        VTrace2=[VTrace2 voltage2];
                    else
                        VTrace2 = [-1 -1];
                    end
                else
                    VTrace2=[-1 -1];
                end
            else
                if diam==1
                    if sum(isnan(voltage2))==0
                        VTrace2=[VTrace2 voltage2];
                    else
                        VTrace2 = [-1];
                    end
                else
                    VTrace2 = [-1 -1];
                end
            end
        else
            VTrace2=[-1 -1];
        end
        %End VTrace2
    end
    %End Diam Loop
    if(sum(CV==-1)==0 & length(VTrace(isnan(VTrace)))==0 & length(VTrace>2)& length(VTrace2(isnan(VTrace2)))==0 & length(VTrace2>2))
        ParticleIDs=[ParticleIDs k]; %Particles which produce an AP
    end
end
%End CV VTrace Collection

%% Batch Strength-Duration (parallelized internally)
BatchSDNew(ParticleIDs,dt,tstop,swarm,33);


%% Parallelize Threshold
totsims=length(D)*length(ParticleIDs);
parfor i=1:totsims
    indD=mod(i-1,length(D))+1;
    j=floor((i-1)/length(D))+1;
    indPID=mod(j-1,length(ParticleIDs))+1;
    BatchThresh(10,0,D(indD),modelType,ParticleIDs(indPID),dt,tstop,swarm(end,ParticleIDs(indPID)).loc,24,0,-1);
end

%% Parallelize Recovery Cycle - edit to only do 2 threshold detections and 2 if fires
totsims=length(D)*length(ParticleIDs)*(length(IPI));
parfor i=1:totsims
    indD=mod(i-1,length(D))+1;
    j=floor((i-1)/length(D))+1;
    indPID=mod(j-1,length(ParticleIDs))+1;
    indIPI=floor((j-1)/length(ParticleIDs))+1;
    BatchThresh(0.1,IPI(indIPI),D(indD),modelType,ParticleIDs(indPID),dt,IPI(indIPI)+tstop,swarm(end,ParticleIDs(indPID)).loc,33,-1,8);
end

end

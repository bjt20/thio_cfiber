function BatchSDNew(ParticleID,dt,tstop,swarm,celsius)
% BatchSDNew finds intra and extracellular thresholds
% ParticleID - number denoting which particle we are running
% potentially can move parfor loop out if not possible to do it in here

global modelType PW D

%% Set parameters
id=[];
idPP=[];

%% I think this is obsolete
%if ParticleID>50
%    PWintra=[0.05 0.1 0.2];
%else
%    PWintra=[0.1];
%end

PWintra = [0.1];

totsims=length(D)*length(ParticleID)*(length(PWintra)+length(PW));

parfor i=1:totsims
    indD=mod(i-1,length(D))+1;
    j=floor((i-1)/length(D))+1;
    indPID=mod(j-1,length(ParticleID))+1;
    indPW=floor((j-1)/length(ParticleID))+1;
    if indPW<=length(PWintra)
        BatchThresh(PWintra(indPW),0,D(indD),modelType,ParticleID(indPID),dt,tstop,swarm(end,ParticleID(indPID)).loc,celsius,-1,-1)
    else
        BatchSDExtra(PW(indPW-length(PWintra)),D(indD),modelType,ParticleID(indPID),dt,tstop,swarm(end,ParticleID(indPID)).loc,celsius)
    end
end

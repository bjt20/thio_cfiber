function ParticleIDs = RunAutonomicSims(swarm,numDim,D,modelType,dt,PW,tstop,IPI)
% RunAutonomicSims is used to gather the data for CV, SD, and RC analysis

global numDim D modelType IPI

dt = 0.01;
tstop = 20;

%% Parallelize CV
parfor r=1:numDim
    for q=1:length(D)
        BatchCV(D(q),modelType,r,dt,1000,37,swarm(end,r).loc);
    end
end

%% Check for broken particles and collect CV/VTrace data
ParticleIDs=[];
for k=1:numDim
    VTrace=[];
    for diam=1:length(D)
        %CV
        name=['CV/c_fiber_CV_37_C_' num2str(D(diam)*1000) '_D_' num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            data=load(name);
            if isempty(data)
                CV(diam)=-1;
            else
                CV(diam)=data;
                CV(CV==0)=-1;
            end
        else
            CV(diam)=-1;
        end
        %End CV

        % Voltage Trace
        name=['VoltageTrace/c_fiber_37_C_' num2str(D(diam)*1000) '_D_'...
            num2str(modelType) '_Type_' num2str(k) '.dat'];
        if exist(name)
            voltage=load(name);
            if ~isempty(VTrace)
                if length(voltage)==length(VTrace)
                    VTrace=[VTrace voltage];
                else
                    VTrace=[VTrace VTrace.*0+-1];
                end
            else
                if diam==1
                    VTrace=[VTrace voltage];
                else
                    VTrace = [-1 -1];
                end
            end
        else
            VTrace=[-1 -1];
        end
        %End Voltage Trace
    end
    %End Diam Loop
    if(sum(CV==-1)==0 & length(VTrace(isnan(VTrace)))==0 & length(VTrace>2))
        ParticleIDs=[ParticleIDs k]; %Particles which produce an AP
    end
end
%End CV VTrace Collection

%% Batch Strength-Duration (parallelized internally)
BatchSDNew(ParticleIDs,dt,tstop,swarm,37);

%% Parallelize Recovery Cycle - edit to only do 2 threshold detections and 2 if fires
totsims=length(D)*length(ParticleIDs)*(length(IPI));
parfor i=1:totsims
    indD=mod(i-1,length(D))+1;
    j=floor((i-1)/length(D))+1;
    indPID=mod(j-1,length(ParticleIDs))+1;
    indIPI=floor((j-1)/length(ParticleIDs))+1;
    BatchThresh(0.1,IPI(indIPI),D(indD),modelType,ParticleIDs(indPID),dt,IPI(indIPI)+tstop,swarm(end,ParticleIDs(indPID)).loc,37,-1,5);
end

end

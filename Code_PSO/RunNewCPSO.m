function RunNewCPSO(versionin,analogversionin,modelTypein,startingIterationin,smartPropin)

global version analogversion modelType startingIteration smartProp idealOutcomes numOutcomes PW D IPI

version = versionin;
analogversion = analogversionin;
modelType = modelTypein;
startingIteration = startingIterationin;
smartProp = smartPropin;

%Fiber specific version differences
if (version == 1) %autonomic
    %minv> cv chron ppt apw spt zct rpt endv<
    idealOutcomes = [-75 1.1 1.15 0.85 2.1 1.07 1 1 -45];

    % list of pulse widths in ms
    PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 2 10];

    % list of diameters in um
    D = [0.5 2];

    % list of IPI in ms
    IPI = [13 23 51 101 250];
else %cutaneous
    %minv> cv chron pp apw sp ppT spt endv<
    idealOutcomes = [-75 1 1 1 2.05 1.25 0.075 -45];

    % list of pulse widths in ms
    PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 2 10];

    % list of diameters in um
    D = [1];%[0.5 1.5];

    % list of IPI in ms
    IPI = [5:5:30 150];
end
numOutcomes = length(idealOutcomes)-2;

pause(1);

pso_optimize;

end

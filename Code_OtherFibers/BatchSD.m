function BatchSD(PW,modelType,ParticleID,D)
% PW to run
% modelType 1:Sundt 2:Tigerholm 3:Rattay 4:Schild
tstop = 70;%simulation duration
len=5000;%Stable[um]
segdensity=50/6;%segment length [um]
initialdel=0; % pulse delay
% D = 1;
% Togglable
dura=PW; % duration of pulse [ms]
type =modelType; % fiber type 1:Sundt 2:Tigerholm 3:Rattay
call_neuron_MATLAB('SD.hoc',segdensity,tstop,len,D,type,dura,initialdel,ParticleID);

%% Load in Base Data
function Runner3(modelType)
addpath('./PSOFunctions');
% list of pulse widths in ms
PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 2 10];
% list of diameters in um
D = [0.5 1 1.5 2];
% list of IPI in ms
IPI = 1:2:250;%;[13 23 51 101 250];
dt=0.01;
tstop=20;
VRest=[-60 -55 -47 -69];

numCPUs = feature('numCores'); % Get number of CPUs available
pc_storage_dir = fullfile('pc_temp_storage',getenv('SLURM_JOB_ID'));    % assign JobStorageLocation based on id of SLURM job that called this function
mkdir(pc_storage_dir);
pc = parcluster('local');
pc.JobStorageLocation = pc_storage_dir;
fprintf('Number of CPUs requested = %g\n',numCPUs);
poolobj = parpool(pc,numCPUs-1); % initialize pool of workers, i.e. CPUs in this node to assign tasks, leave 1 CPU to handle the overhead

if modelType>2 % Autonomic
    parfor q=1:4
        BatchCV(D(q),modelType,1,dt,1000,37,VRest(modelType));
    end
    BatchSDNew2(1,modelType,PW,D,dt,tstop,VRest(modelType));

    %% Parallelize Recovery Cycle - edit to only do 2 threshold detections and 2 if fires
    totsims=length(D)*length(1)*(length(IPI));
    parfor i=1:totsims
        indD=mod(i-1,length(D))+1;
        j=floor((i-1)/length(D))+1;
        indIPI=floor((j-1)/length(1))+1;
        BatchThresh(0.1,IPI(indIPI),D(indD),modelType,1,dt,IPI(indIPI)+tstop,VRest(modelType),37,-1,100);
    end
else % Cutaneous
    parfor q=1:4
        BatchCV(D(q),modelType,1,dt,1000,24,VRest(modelType));
    end
    parfor q=1:4
        BatchCV(D(q),modelType,1,dt,1000,37,VRest(modelType));
        BatchThresh(10,0,D(q),modelType,1,dt,tstop,VRest(modelType),24,0,100);
    end
    BatchSDNew2(1,modelType,PW,D,dt,tstop,VRest(modelType));

    %% Parallelize Recovery Cycle - edit to only do 2 threshold detections and 2 if fires
    totsims=length(D)*length(1)*(length(IPI));
    parfor i=1:totsims
        indD=mod(i-1,length(D))+1;
        j=floor((i-1)/length(D))+1;
        indIPI=floor((j-1)/length(1))+1;
        BatchThresh(0.1,IPI(indIPI),D(indD),modelType,1,dt,IPI(indIPI)+tstop,VRest(modelType),33,-1,100);
    end
end
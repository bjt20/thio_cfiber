function Master(i)
% % Master is used to gather the data for SD, CV, and PP analysis
% % ParticleID - number denoting which particle we are running
% % potentially can move parfor loop out if not possible to do it in here
% %% make directories
% if exist('PairPulse')~=7
%     mkdir('PairPulse')
% end
% if exist('SD')~=7
%     mkdir('SD')
% end
% if exist('CV')~=7
%     mkdir('CV')
% end
%% Set parameters
% modelType 1:Sundt 2:Tigerholm 3:Rattay 4:Schild
% modelType = 3;

% list of pulse widths in ms
PW = [0.01 0.02 0.05 0.1 0.2 0.5 1 2 5 100];

% list of diameters in um
D = [0.5 0.75 1 1.25 1.5];
ParticleID=1;
% list of IPI in ms
IPI = [1:2:20 22:10:102 120:80:520 1000];
for modelType = [4]
%     for i=1:(length(PW))%+length(D)+length(IPI))
        %     if i<=length(PW)
        BatchSD(PW(i),modelType,ParticleID,1);
        %     elseif i<=length(D)
        %         BatchCV(D(i-length(PW)),modelType,ParticleID);
        %     else
        %         BatchPP(IPI(i-length(PW)-length(D)),modelType,ParticleID);
        %     end
%     end
end

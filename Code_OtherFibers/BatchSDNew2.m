function BatchSDNew2(ParticleID,modelType,PW,D,dt,tstop,VRest)
% Master is used to gather the data for SD, CV, and PP analysis
% ParticleID - number denoting which particle we are running
% potentially can move parfor loop out if not possible to do it in here

%% make directories
if exist('Thresh')~=7
    mkdir('Thresh')
end
if exist('SD')~=7
    mkdir('SD')
end

%% Set parameters
tstop=20;
id=[];
idPP=[];

%% I think this is obsolete
%if ParticleID>50
%    PWintra=[0.05 0.1 0.2];
%else
%    PWintra=[0.1];
%end

PWintra = [0.1];

totsims=length(D)*length(ParticleID)*(length(PWintra)+length(PW));

parfor i=1:totsims
    indD=mod(i-1,length(D))+1;
    j=floor((i-1)/length(D))+1;
    indPID=mod(j-1,length(ParticleID))+1;
    indPW=floor((j-1)/length(ParticleID))+1;
    if indPW<=length(PWintra)
        if modelType>2
            BatchThresh(PWintra(indPW),0,D(indD),modelType,ParticleID(indPID),dt,tstop,VRest,37,-1,100)
        else
            BatchThresh(PWintra(indPW),0,D(indD),modelType,ParticleID(indPID),dt,tstop,VRest,33,-1,100)
        end
    else
        if modelType>2
            BatchSDExtra(PW(indPW-length(PWintra)),D(indD),modelType,ParticleID(indPID),dt,tstop,VRest,37)
        else
            BatchSDExtra(PW(indPW-length(PWintra)),D(indD),modelType,ParticleID(indPID),dt,tstop,VRest,33)
        end
    end
end

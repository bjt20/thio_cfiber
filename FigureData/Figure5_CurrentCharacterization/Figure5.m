%% Cutaneous
diams=[1];
d=1;
temp=[37];
t=1;

Voltage=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_37_C_1000_D_5_Type_30.dat']);
nav7=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_Nav7_Full_Extra.dat']);
nav8=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_Nav8_Full_Extra.dat']);
nav9=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_Nav9_Full_Extra.dat']);
kv1=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_Kv1_Full_Extra.dat']);
kv2=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_Kv2_Full_Extra.dat']);
ka3=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_Ka3_Full_Extra.dat']);
kv7=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_Kv7_Full_Extra.dat']);
BK=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_BK_Full_Extra.dat']);
SK=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_SK_Full_Extra.dat']);
cav12=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_CaV1_Full_Extra.dat']);
cav22=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_CaV2_Full_Extra.dat']);
inaHCN=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_inaHCN_Full_Extra.dat']);
ikHCN=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_ikHCN_Full_Extra.dat']);
inaPump=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_inaNaCaPump_Full_Extra.dat']);
ikPump=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_ikNaCaPump_Full_Extra.dat']);
inaNaCaX=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_inaNaCaX_Full_Extra.dat']);
icaNaCaX=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_30_' num2str(temp(t)) '_Temp_icaNaCaX_Full_Extra.dat']);


%% Patch currents
currtot=abs(nav7)+abs(nav8)+abs(nav9)+abs(inaHCN)+abs(cav12)+abs(cav22)+abs(ikPump)+abs(kv1)+abs(kv2)+abs(ka3)+abs(kv7)+abs(BK)+abs(SK)+abs(ikHCN)+abs(inaPump)+abs(inaNaCaX)+abs(icaNaCaX);
nav7p=nav7./currtot;
nav8p=nav8./currtot;
nav9p=nav9./currtot;
hcnp=inaHCN./currtot;
cav12p=cav12./currtot;
cav22p=cav22./currtot;
ikPumpp=ikPump./currtot;

time=linspace(0,100,length(nav8p));
figure
% p=polyshape([0 0 5000 5000],[0 1 1 0]);
% plot(p)
hold on
x=[0 time(1:10001) 100];
y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)'+cav12p(1:10001)'+cav22p(1:10001)'+ikPumpp(1:10001)'+inaNaCaX(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)'+cav12p(1:10001)'+cav22p(1:10001)'+ikPumpp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)'+cav12p(1:10001)'+cav22p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)'+cav12p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

kv1p=kv1./currtot;
kv2p=kv2./currtot;
ka3p=ka3./currtot;
kv7p=kv7./currtot;
BKp=BK./currtot;
SKp=SK./currtot;
HCNp=ikHCN./currtot;
inaPumpp=inaPump./currtot;
icaNaCaXp=icaNaCaX./currtot;

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)'+SKp(1:10001)'+HCNp(1:10001)'+inaPumpp(1:10001)'+icaNaCaXp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)'+SKp(1:10001)'+HCNp(1:10001)'+inaPumpp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)'+SKp(1:10001)'+HCNp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)'+SKp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)' 0];
p=polyshape(x,y)
plot(p)
axis([0 10 -1 1]) % Change based on time you care about
xlabel('Time (ms)')
ylabel('Proportion of Total Current')
fileName='Figures\Proportion_Cutaneous_Short.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%%
figure
plot(linspace(0,100,10001),Voltage(1:end-1))
axis([0 10 -80 80])% Change based on time you care about
fileName='Figures\Proportion_Cutaneous_Vm_Short.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

figure
plot(linspace(0,100,10001),Voltage(1:end-1))
axis([10 100 -80 80])% Change based on time you care about
fileName='Figures\Proportion_Cutaneous_Vm_Long.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%% Autonomic
diams=[1];
d=1;
temp=[37];
t=1;

Voltage=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_37_C_1000_D_5_Type_14.dat']);
nav7=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_Nav7_Full_Extra.dat']);
nav8=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_Nav8_Full_Extra.dat']);
nav9=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_Nav9_Full_Extra.dat']);
kv1=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_Kv1_Full_Extra.dat']);
kv2=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_Kv2_Full_Extra.dat']);
ka3=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_Ka3_Full_Extra.dat']);
kv7=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_Kv7_Full_Extra.dat']);
BK=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_BK_Full_Extra.dat']);
SK=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_SK_Full_Extra.dat']);
cav12=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_CaV1_Full_Extra.dat']);
cav22=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_CaV2_Full_Extra.dat']);
inaHCN=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_inaHCN_Full_Extra.dat']);
ikHCN=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_ikHCN_Full_Extra.dat']);
inaPump=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_inaNaCaPump_Full_Extra.dat']);
ikPump=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_ikNaCaPump_Full_Extra.dat']);
inaNaCaX=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_inaNaCaX_Full_Extra.dat']);
icaNaCaX=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_' num2str(diams(d).*1000) '_D_5_Type_14_' num2str(temp(t)) '_Temp_icaNaCaX_Full_Extra.dat']);


%% Patch currents
currtot=abs(nav7)+abs(nav8)+abs(nav9)+abs(inaHCN)+abs(cav12)+abs(cav22)+abs(ikPump)+abs(kv1)+abs(kv2)+abs(ka3)+abs(kv7)+abs(BK)+abs(SK)+abs(ikHCN)+abs(inaPump)+abs(inaNaCaX)+abs(icaNaCaX);
nav7p=nav7./currtot;
nav8p=nav8./currtot;
nav9p=nav9./currtot;
hcnp=inaHCN./currtot;
cav12p=cav12./currtot;
cav22p=cav22./currtot;
ikPumpp=ikPump./currtot;

time=linspace(0,100,length(nav8p));
figure
% p=polyshape([0 0 5000 5000],[0 1 1 0]);
plot(p)
hold on
x=[0 time(1:10001) 100];
y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)'+cav12p(1:10001)'+cav22p(1:10001)'+ikPumpp(1:10001)'+inaNaCaX(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)'+cav12p(1:10001)'+cav22p(1:10001)'+ikPumpp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)'+cav12p(1:10001)'+cav22p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)'+cav12p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)'+hcnp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)'+nav9p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)'+nav8p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 nav7p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

kv1p=kv1./currtot;
kv2p=kv2./currtot;
ka3p=ka3./currtot;
kv7p=kv7./currtot;
BKp=BK./currtot;
SKp=SK./currtot;
HCNp=ikHCN./currtot;
inaPumpp=inaPump./currtot;
icaNaCaXp=icaNaCaX./currtot;

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)'+SKp(1:10001)'+HCNp(1:10001)'+inaPumpp(1:10001)'+icaNaCaXp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)'+SKp(1:10001)'+HCNp(1:10001)'+inaPumpp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)'+SKp(1:10001)'+HCNp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)'+SKp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)'+BKp(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)'+kv7p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)'+ka3p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)'+kv2p(1:10001)' 0];
p=polyshape(x,y)
plot(p)

y=[0 kv1p(1:10001)' 0];
p=polyshape(x,y)
plot(p)
axis([0 10 -1 1]) % Change based on time you care about
xlabel('Time (ms)')
ylabel('Proportion of Total Current')
fileName='Figures\Proportion_Autonomic_Long.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

%%
figure
plot(linspace(0,100,10001),Voltage(1:end-1))
axis([0 10 -80 80])% Change based on time you care about
fileName='Figures\Proportion_Autonomic_Vm_Short.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

figure
plot(linspace(0,100,10001),Voltage(1:end-1))
axis([10 100 -80 80])% Change based on time you care about
fileName='Figures\Proportion_Autonomic_Vm_Long.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
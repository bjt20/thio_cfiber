%% ADS Ion Currents Autonomic
ko=load('Autonomic/AAT12New3/ADS/c_fiber_2000_D_5_Type_40_Freq_0_delay_33_37_Temp_koTrace_2.dat');
nai=load('Autonomic/AAT12New3/ADS/c_fiber_2000_D_5_Type_40_Freq_0_delay_33_37_Temp_NaiTrace_2.dat');

figure
plot(linspace(0,180,length(nai(1:10:end))),nai(1:10:end))
hold on
plot(linspace(0,180,length(ko(1:10:end))),ko(1:10:end))
axis([0 180 0 30])
xlabel('Time (s)')
ylabel('Concentration (mM)')
legend('Intracellular Na^+','Periaxonal K^+')
fileName=['..\Figures\ADSIonConcentrations_2um.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf');
%% ADS Ion Currents Cutaneous
ko=load('Cutaneous/ACT3/ADS/c_fiber_1500_D_5_Type_40_Freq_0_delay_39_37_Temp_koTrace_2.dat');
nai=load('Cutaneous/ACT3/ADS/c_fiber_1500_D_5_Type_40_Freq_0_delay_39_37_Temp_NaiTrace_2.dat');

figure
plot(linspace(0,180,length(nai(1:10:end))),nai(1:10:end))
hold on
plot(linspace(0,180,length(ko(1:10:end))),ko(1:10:end))
axis([0 180 0 40])
xlabel('Time (s)')
ylabel('Concentration (mM)')
legend('Intracellular Na^+','Periaxonal K^+')
fileName=['..\Figures\ADSIonConcentrations_2um.pdf'];
% print(gcf,fileName,'-bestfit','-dpdf');
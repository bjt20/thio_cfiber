addpath ../../AnalysisFtns
%% Autonomic
perf=[];
perfb=[];
for i=1:231
    load(['..\Data_CFiber\Autonomic\Elect\Outcomes' num2str(i) '.mat'])
    perf=[perf min(sum(abs(potential)))];
    perfb=[perfb max(sum(abs(binarypotential)))];
end
%%
figure
plot(perf)
where=[1 2 91 231];
vals=[2 5 6 7];
hold on
plot(where,[120 120 120 120],'ro')
xlabel('Iteration #')
ylabel('Min Cost')
% print('Figures\Autonomic_ElectCost.pdf','-dpdf')
%% Cutaneous
perf=[];
perfb=[];
for i=1:543
    load(['..\Data_CFiber\Cutaneous\Elect\Outcomes' num2str(i) '.mat'])
    perf=[perf min(sum(abs(potential)))];
    perfb=[perfb max(sum(abs(binarypotential)))];
end
%%
figure
plot(perf)
where=[1 2 8 42 153 543];
vals=[1 2 3 4 5 6];
hold on
plot(where,[60 60 60 60 60 60],'ro')
xlabel('Iteration #')
ylabel('Min Cost')
% print('Figures\Cutaneous_ElectCost.pdf','-dpdf')
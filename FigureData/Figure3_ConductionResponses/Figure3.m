addpath ../../AnalysisFtns
%% CV
figure
v2 = [0 0.45; 0 1.76;4 1.76; 4 0.45];
f2 = [1 2 3 4];
patch('Faces',f2,'Vertices',v2,'FaceColor','black','FaceAlpha',.1);
hold on
for i=14
    CV_5=load(['../Data_CFiber/Autonomic/CV/c_fiber_CV_37_C_500_D_5_Type_' num2str(i) '.dat']);
    CV_10=load(['../Data_CFiber/Autonomic/CV/c_fiber_CV_37_C_1000_D_5_Type_' num2str(i) '.dat']);
    CV_15=load(['../Data_CFiber/Autonomic/CV/c_fiber_CV_37_C_1500_D_5_Type_' num2str(i) '.dat']);
    CV_20=load(['../Data_CFiber/Autonomic/CV/c_fiber_CV_37_C_2000_D_5_Type_' num2str(i) '.dat']);
    plot([1 ],[ CV_10 ],'o')
    errorbar(1,CV_10,CV_10-CV_5,CV_15-CV_10,'LineWidth',2)
end
CV_5=load(['../Data_CFiber/Schild94/CV/c_fiber_CV_37_C_500_D_3_Type_1.dat']);
CV_10=load(['../Data_CFiber/Schild94/CV/c_fiber_CV_37_C_1000_D_3_Type_1.dat']);
CV_15=load(['../Data_CFiber/Schild94/CV/c_fiber_CV_37_C_1500_D_3_Type_1.dat']);
CV_20=load(['../Data_CFiber/Schild94/CV/c_fiber_CV_37_C_2000_D_3_Type_1.dat']);
plot([2 ],[ CV_10 ],'o')
errorbar(2,CV_10,CV_10-CV_5,CV_15-CV_10,'LineWidth',2)

CV_5=load(['../Data_CFiber/Schild97/CV/c_fiber_CV_37_C_500_D_4_Type_1.dat']);
CV_10=load(['../Data_CFiber/Schild97/CV/c_fiber_CV_37_C_1000_D_4_Type_1.dat']);
CV_15=load(['../Data_CFiber/Schild97/CV/c_fiber_CV_37_C_1500_D_4_Type_1.dat']);
CV_20=load(['../Data_CFiber/Schild97/CV/c_fiber_CV_37_C_2000_D_4_Type_1.dat']);
plot([3 ],[ CV_10 ],'o')
errorbar(3,CV_10,CV_10-CV_5,CV_15-CV_10,'LineWidth',2)

v2 = [4 0.5; 4 1.5;8 1.5; 8 0.5];
f2 = [1 2 3 4];
patch('Faces',f2,'Vertices',v2,'FaceColor','black','FaceAlpha',.1);
hold on
runs=5;
IDs=30;
for i=1
    CV_5=load(['../Data_CFiber/Cutaneous/CV/c_fiber_CV_37_C_500_D_5_Type_' num2str(IDs(i)) '.dat']);
    CV_10=load(['../Data_CFiber/Cutaneous/CV/c_fiber_CV_37_C_1000_D_5_Type_' num2str(IDs(i)) '.dat']);
    CV_20=load(['../Data_CFiber/Cutaneous/CV/c_fiber_CV_37_C_1500_D_5_Type_' num2str(IDs(i)) '.dat']);
    plot([5],[CV_10],'o')
    errorbar(5,CV_10,CV_10-CV_5,CV_20-CV_10,'LineWidth',2)
end

CV_5=load(['../Data_CFiber/Sundt/CV/c_fiber_CV_37_C_500_D_1_Type_1.dat']);
CV_10=load(['../Data_CFiber/Sundt/CV/c_fiber_CV_37_C_1000_D_1_Type_1.dat']);
CV_15=load(['../Data_CFiber/Sundt/CV/c_fiber_CV_37_C_1500_D_1_Type_1.dat']);
CV_20=load(['../Data_CFiber/Sundt/CV/c_fiber_CV_37_C_2000_D_1_Type_1.dat']);
plot([6 ],[ CV_10 ],'o')
errorbar(6,CV_10,CV_10-CV_5,CV_15-CV_10,'LineWidth',2)

CV_5=load(['../Data_CFiber/Tigerholm/CV/c_fiber_CV_37_C_500_D_2_Type_1.dat']);
CV_10=load(['../Data_CFiber/Tigerholm/CV/c_fiber_CV_37_C_1000_D_2_Type_1.dat']);
CV_15=load(['../Data_CFiber/Tigerholm/CV/c_fiber_CV_37_C_1500_D_2_Type_1.dat']);
CV_20=load(['../Data_CFiber/Tigerholm/CV/c_fiber_CV_37_C_2000_D_2_Type_1.dat']);
plot([7 ],[ CV_10 ],'o')
errorbar(7,CV_10,CV_10-CV_5,CV_15-CV_10,'LineWidth',2)

axis([0 8 0 2])
fileName='Figures\CV_Final_Cutaneous_New.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

%% Chronaxie
PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 2 10];
n=[14];

for j=1:length(n)
    data5=[];
    data10=[];
    data15=[];
    data20=[];
    for i=1:length(PW)
        data5=[data5 load(['../Data_CFiber/Autonomic/SD/c_fiber_Thresh_37_C_500_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(n(j)) '.dat'])];
        data10=[data10 load(['../Data_CFiber/Autonomic/SD/c_fiber_Thresh_37_C_1000_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(n(j)) '.dat'])];
        data15=[data15 load(['../Data_CFiber/Autonomic/SD/c_fiber_Thresh_37_C_1500_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(n(j)) '.dat'])];
        data20=[data20 load(['../Data_CFiber/Autonomic/SD/c_fiber_Thresh_37_C_2000_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(n(j)) '.dat'])];
    end
    c5up(j)=findChronaxieOnly(PW,data5);
    c10up(j)=findChronaxieOnly(PW,data10);
    c15up(j)=findChronaxieOnly(PW,data15);
    c20up(j)=findChronaxieOnly(PW,data20);
end
data5=[];
data10=[];
data15=[];
data20=[];
for i=1:length(PW)
    data5=[data5 load(['../Data_CFiber/Schild94/SD/c_fiber_Thresh_37_C_500_D_' num2str(PW(i).*1000) '_Duration_3_Type_1.dat'])];
    data10=[data10 load(['../Data_CFiber/Schild94/SD/c_fiber_Thresh_37_C_1000_D_' num2str(PW(i).*1000) '_Duration_3_Type_1.dat'])];
    data15=[data15 load(['../Data_CFiber/Schild94/SD/c_fiber_Thresh_37_C_1500_D_' num2str(PW(i).*1000) '_Duration_3_Type_1.dat'])];
    data20=[data20 load(['../Data_CFiber/Schild94/SD/c_fiber_Thresh_37_C_2000_D_' num2str(PW(i).*1000) '_Duration_3_Type_1.dat'])];
end
c5up(2)=findChronaxieOnly(PW,data5);
c10up(2)=findChronaxieOnly(PW,data10);
c15up(2)=findChronaxieOnly(PW,data15);
c20up(2)=findChronaxieOnly(PW,data20);

data5=[];
data10=[];
data15=[];
data20=[];
for i=1:length(PW)
    data5=[data5 load(['../Data_CFiber/Schild97/SD/c_fiber_Thresh_37_C_500_D_' num2str(PW(i).*1000) '_Duration_4_Type_1.dat'])];
    data10=[data10 load(['../Data_CFiber/Schild97/SD/c_fiber_Thresh_37_C_1000_D_' num2str(PW(i).*1000) '_Duration_4_Type_1.dat'])];
    data15=[data15 load(['../Data_CFiber/Schild97/SD/c_fiber_Thresh_37_C_1500_D_' num2str(PW(i).*1000) '_Duration_4_Type_1.dat'])];
    data20=[data20 load(['../Data_CFiber/Schild97/SD/c_fiber_Thresh_37_C_2000_D_' num2str(PW(i).*1000) '_Duration_4_Type_1.dat'])];
end
c5up(3)=findChronaxieOnly(PW(data5>0),data5(data5>0));
c10up(3)=findChronaxieOnly(PW(data10>0),data10(data10>0));
c15up(3)=findChronaxieOnly(PW(data15>0),data15(data15>0));
c20up(3)=findChronaxieOnly(PW(data20>0),data20(data20>0));

PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 2 10];
runs=5;
IDs=30;
for j=1
    data5=[];
    data10=[];  
    data20=[];
    for i=1:length(PW)
        data5=[data5 load(['../Data_CFiber/Cutaneous/SD/c_fiber_Thresh_33_C_500_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(IDs(j)) '.dat'])];
        data10=[data10 load(['../Data_CFiber/Cutaneous/SD/c_fiber_Thresh_33_C_1000_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(IDs(j)) '.dat'])];
        data20=[data20 load(['../Data_CFiber/Cutaneous/SD/c_fiber_Thresh_33_C_1500_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(IDs(j)) '.dat'])];
    end
    c5up(4)=findChronaxieOnly(PW,data5);
    c10up(4)=findChronaxieOnly(PW,data10);
    c15up(4)=findChronaxieOnly(PW,data20);
end

data5=[];
data10=[];
data15=[];
data20=[];
for i=1:length(PW)
    data5=[data5 load(['../Data_CFiber/Sundt/SD/c_fiber_Thresh_33_C_500_D_' num2str(PW(i).*1000) '_Duration_1_Type_1.dat'])];
    data10=[data10 load(['../Data_CFiber/Sundt/SD/c_fiber_Thresh_33_C_1000_D_' num2str(PW(i).*1000) '_Duration_1_Type_1.dat'])];
    data15=[data15 load(['../Data_CFiber/Sundt/SD/c_fiber_Thresh_33_C_1500_D_' num2str(PW(i).*1000) '_Duration_1_Type_1.dat'])];
    data20=[data20 load(['../Data_CFiber/Sundt/SD/c_fiber_Thresh_33_C_2000_D_' num2str(PW(i).*1000) '_Duration_1_Type_1.dat'])];
end
c5up(5)=findChronaxieOnly(PW,data5);
c10up(5)=findChronaxieOnly(PW,data10);
c15up(5)=findChronaxieOnly(PW,data15);
c20up(5)=findChronaxieOnly(PW,data20);

data5=[];
data10=[];
data15=[];
data20=[];
for i=1:length(PW)
    data5=[data5 load(['../Data_CFiber/Tigerholm/SD/c_fiber_Thresh_33_C_500_D_' num2str(PW(i).*1000) '_Duration_2_Type_1.dat'])];
    data10=[data10 load(['../Data_CFiber/Tigerholm/SD/c_fiber_Thresh_33_C_1000_D_' num2str(PW(i).*1000) '_Duration_2_Type_1.dat'])];
    data15=[data15 load(['../Data_CFiber/Tigerholm/SD/c_fiber_Thresh_33_C_1500_D_' num2str(PW(i).*1000) '_Duration_2_Type_1.dat'])];
    data20=[data20 load(['../Data_CFiber/Tigerholm/SD/c_fiber_Thresh_33_C_2000_D_' num2str(PW(i).*1000) '_Duration_2_Type_1.dat'])];
end
c5up(6)=findChronaxieOnly(PW(data5>0),data5(data5>0));
c10up(6)=findChronaxieOnly(PW(data10>0),data10(data10>0));
c15up(6)=findChronaxieOnly(PW(data15>0),data15(data15>0));
c20up(6)=findChronaxieOnly(PW(data20>0),data20(data20>0));

%% Plot Chronaxies Autonomic
figure
hold on
PW=10:10:1000;
I1=1+1500./PW;
I2=1+750./PW;
v2 = [PW' I1';PW(end:-1:1)' I2(end:-1:1)'];
f2 = 1:length(PW)*2;

load ../ValidationData/Koslow1973_Fig4.mat

[r1,~]=findChronaxie(Koslow1973_Fig4_mean(:,1),Koslow1973_Fig4_mean(:,2));
[r1L,~]=findChronaxie(Koslow1973_Fig4_LB(:,1),Koslow1973_Fig4_LB(:,2));
[r1U,~]=findChronaxie(Koslow1973_Fig4_UB(:,1),Koslow1973_Fig4_UB(:,2));

load ../ValidationData/Woodbury1990_Fig2A_Ctype.mat
[r2,~]=findChronaxie(Woodbury1990_Fig2A_Ctype(:,1),Woodbury1990_Fig2A_Ctype(:,2));

errorbar(Koslow1973_Fig4_mean(:,1).*1000,Koslow1973_Fig4_mean(:,2)./r1,Koslow1973_Fig4_mean(:,2)./r1-Koslow1973_Fig4_LB(:,2)./r1,Koslow1973_Fig4_UB(:,2)./r1-Koslow1973_Fig4_mean(:,2)./r1,'Color','k')
plot(Woodbury1990_Fig2A_Ctype(:,1),Woodbury1990_Fig2A_Ctype(:,2)./r2,'ko')

colors=parula(4);
i=1;
Chron5=1+c5up(i)./PW.*1000;
Chron10=1+c10up(i)./PW.*1000;
plot(PW,Chron10,'-','Color',colors(i,:),'LineWidth',2)
Chron20=1+c15up(i)./PW.*1000;


v2 = [PW' Chron5';PW(end:-1:1)' Chron20(end:-1:1)'];
f2 = 1:length(PW)*2;
patch('Faces',f2,'Vertices',v2,'FaceColor',colors(i,:),'FaceAlpha',.1);

i=2;
Chron10=1+c10up(i)./PW.*1000;
plot(PW,Chron10,'-','Color',colors(i,:),'LineWidth',2)


i=3;
Chron10=1+c10up(i)./PW.*1000;
plot(PW,Chron10,'-','Color',colors(i,:),'LineWidth',2)

axis([0 1000 0 16])
xlabel('PW (\mus)')
ylabel('Ith/Irh')
legend('Koslow 1973','Woodbury 1990','PSO','PSO Range','Schild 1994','Schild 1997')
fileName='Figures\SD_Final_AllRuns_Autonomic.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

%% Plot Chronaxies Cutaneous
figure
hold on
PW=10:10:1000;

load ../ValidationData/LiAndBak_1975.mat
[r1,c1]=findChronaxie(LiAndBak_1975(1:end-1,1),LiAndBak_1975(1:end-1,2));
plot(LiAndBak_1975(:,1).*1000,LiAndBak_1975(:,2)./(LiAndBak_1975(end,2)),'ko')

load ../ValidationData/MerrillEtAl_1978.mat
[r2,c2]=findChronaxie(MerrillEtAl_1978(1:6,1),MerrillEtAl_1978(1:6,2));
plot(MerrillEtAl_1978(1:6,1),MerrillEtAl_1978(1:6,2)./r2,'k*')

colors=parula(8);
i=4;
Chron5=1+c5up(i)./PW.*1000;
Chron10=1+c10up(i)./PW.*1000;
plot(PW,Chron10,'-','Color',colors(i,:),'LineWidth',2)
Chron15=1+c15up(i)./PW.*1000;
v2 = [PW' Chron5';PW(end:-1:1)' Chron15(end:-1:1)'];
f2 = 1:length(PW)*2;
patch('Faces',f2,'Vertices',v2,'FaceColor',colors(i,:),'FaceAlpha',.1);

i=5;
Chron10=1+c10up(i)./PW.*1000;
plot(PW,Chron10,'-','Color',colors(i,:),'LineWidth',2)

i=6;
Chron10=1+c10up(i)./PW.*1000;
plot(PW,Chron10,'-','Color',colors(i,:),'LineWidth',2)

[r3,c3]=findChronaxie(MerrillEtAl_1978(7:end,1),MerrillEtAl_1978(7:end,2));
plot(MerrillEtAl_1978(7:end,1),MerrillEtAl_1978(7:end,2)./r3,'k*')

axis([0 1000 0 16])
xlabel('PW (\mus)')
ylabel('Ith/Irh')
legend('Li and Bak 1975','Merrill et al. 1978','PSO','PSO Range','Sundt','Tigerholm')
fileName='Figures\SD_Final_AllRuns_Cutaneous_Final.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

%% Pair Pulse Recovery Cycle Autonomic
IPI=[1:2:98 100:10:250];

figure()
load('..\ValidationData\Grundfest1938_Fig10.mat')
% Sort data points in order of ascending ISI
tmp = Grundfest1938_Fig10(:,1);
[~,ind] = sort(tmp);
Grundfest1938_Fig10 = Grundfest1938_Fig10(ind,:);
% Redefine y axis:
% 100% excitability = 0% change in threshold
% 0% excitability   = inf threshold
% Decreased excitability = increase in threshold
figure()
tmp = Grundfest1938_Fig10(:,2);
Grundfest1938_Fig10(:,2) = 100.*(100./tmp - 1);
% Plot
plot(Grundfest1938_Fig10(:,1),Grundfest1938_Fig10(:,2),'ks')
hold on
IPI=3:2:250;
colors=parula(8);
IPIs5=[];
IPIs10=[];
IPIs20=[];
for n=14
    ct=1;
    start5(1)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_500_D_100_Duration_0_Delay_5_Type_' num2str(n) '.dat']);
    start10(1)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_0_Delay_5_Type_' num2str(n) '.dat']);
    start20(1)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1500_D_100_Duration_0_Delay_5_Type_' num2str(n) '.dat']);
    for i=IPI
        IPIs5(1,ct)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_500_D_100_Duration_' num2str(i) '_Delay_5_Type_' num2str(n) '.dat']);
        IPIs10(1,ct)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(i) '_Delay_5_Type_' num2str(n) '.dat']);
        IPIs20(1,ct)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1500_D_100_Duration_' num2str(i) '_Delay_5_Type_' num2str(n) '.dat']);
        ct=ct+1;
    end
end

PP5Norm=(IPIs5./start5'-1).*100;
PP5Norm(PP5Norm>49)=100;

PP10Norm=(IPIs10./start10'-1).*100;
PP10Norm(PP10Norm>49)=100;

PP20Norm=(IPIs20./start20'-1).*100;
PP20Norm(PP20Norm>49)=100;
for i=1
    plot(IPI,PP10Norm(i,:),'-','Color',colors(i,:))
end

v2 = [IPI' PP5Norm';IPI(end:-1:1)' PP20Norm(end:-1:1)'];
f2 = 1:length(IPI)*2;
patch('Faces',f2,'Vertices',v2,'FaceColor',colors(i,:),'FaceAlpha',.1);

IPIs5=[];
IPIs10=[];
IPIs20=[];
for n=1
    ct=1;
    start10(1)=load(['../Data_CFiber/Schild94/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_0_Delay_3_Type_' num2str(n) '.dat']);
    start10(2)=load(['../Data_CFiber/Schild97/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_0_Delay_4_Type_' num2str(n) '.dat']);
    for i=IPI
        if ~isempty(load(['../Data_CFiber/Schild94/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(i) '_Delay_3_Type_' num2str(n) '.dat']))
            IPIs10(1,ct)=load(['../Data_CFiber/Schild94/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(i) '_Delay_3_Type_' num2str(n) '.dat']);
        else
            IPIs10(1,ct)=100;
        end

        if ~isempty(load(['../Data_CFiber/Schild97/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(i) '_Delay_4_Type_' num2str(n) '.dat']))
            IPIs10(2,ct)=load(['../Data_CFiber/Schild97/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(i) '_Delay_4_Type_' num2str(n) '.dat']);
        else
            IPIs10(2,ct)=100;
        end
        
        ct=ct+1;
    end
end

PP10Norm=(IPIs10(1,:)./start10(1)'-1).*100;
PP10Norm(PP10Norm>49)=100;

for i=1
    plot(IPI,PP10Norm(i,:),'-','Color',colors(2,:))
end

PP10Norm=(IPIs10(2,:)./start10(2)'-1).*100;
PP10Norm(PP10Norm>49)=100;

for i=1
    plot(IPI,PP10Norm(i,:),'-','Color',colors(3,:))
end


axis([0 250 -50 50])
plot([0 500],[0 0],'LineWidth',2)
title('Recovery Cycle','FontSize',16)
xlabel('Inter Stimulus Interval (ms)','FontSize',16)
ylabel('%Change in Threshold','FontSize',16)
legend('Grundfest 1938','PSO','Range','Schild 1994','Schild 1997','Zero line')
fileName='Figures\RC_Final_AllRuns_Auto_Before.pdf';
err=sqrt(sum(min((Grundfest1938_Fig10(:,2)-interp1(IPI,PP5Norm,Grundfest1938_Fig10(:,1))).^2,(Grundfest1938_Fig10(:,2)-interp1(IPI,PP20Norm,Grundfest1938_Fig10(:,1))).^2)))

% print(gcf,fileName,'-bestfit','-dpdf');
%% Threshold RC Cutaneous
% ***** C-type
figure
load ../ValidationData/RCData.mat
plot(RC1(:,1),RC1(:,2),'ks')
hold on
IPI=[3:2:250];
colors=parula(4);
diams=[0.5 1 1.5];
PPNorms=[];
for d=diams
    for j=1
        PP=[];
        runs=5;
        IDs=30;
        ParticleID=IDs(j);
        include=[];
        for i=1:length(IPI)
            if j==0
                name=['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_' num2str(d*1000) '_D_100_Duration_' num2str(IPI(i)) '_Delay_5_Type_' num2str(ParticleID) '.dat'];
            else
                name=['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_' num2str(d*1000) '_D_100_Duration_' num2str(IPI(i)) '_Delay_5_Type_' num2str(ParticleID) '.dat'];
            end
            
            if exist(name)
                include=[include i];
                PP=[PP load(name)];
            end
        end
        if j==0
            Intra=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_' num2str(d*1000) '_D_100_Duration_0_Delay_5_Type_' num2str(ParticleID) '.dat']);
        else
            Intra=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_' num2str(d*1000) '_D_100_Duration_0_Delay_5_Type_' num2str(ParticleID) '.dat']);
        end
        
        PPNorm=(PP(:)./Intra-1).*100;
        PPNorm(PPNorm>49)=100;
        PPNorms=[PPNorms PPNorm];
        
        % axis([0 500 -100 100])
    end
end
plot(IPI(include),PPNorms(:,2),'Color',colors(j+1,:),'LineWidth',2)
v2 = [IPI' PPNorms(:,1);IPI(end:-1:1)' PPNorms(end:-1:1,3)];
f2 = 1:length(IPI)*2;
patch('Faces',f2,'Vertices',v2,'FaceColor',colors(1,:),'FaceAlpha',.1);

IPIs5=[];
IPIs10=[];
IPIs20=[];
for n=1
    ct=1;
    start10(1)=load(['../Data_CFiber/Sundt/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_0_Delay_1_Type_' num2str(n) '.dat']);
    start10(2)=load(['../Data_CFiber/Tigerholm/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_0_Delay_2_Type_' num2str(n) '.dat']);
    for i=IPI
        IPIs10(1,ct)=load(['../Data_CFiber/Sundt/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_' num2str(i) '_Delay_1_Type_' num2str(n) '.dat']);
        IPIs10(2,ct)=load(['../Data_CFiber/Tigerholm/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_' num2str(i) '_Delay_2_Type_' num2str(n) '.dat']);
        ct=ct+1;
    end
end

PP10Norm=(IPIs10(1,:)./start10(1)'-1).*100;
PP10Norm(PP10Norm>49)=100;

for i=2
    plot(IPI,PP10Norm(1,:),'-','Color',colors(i,:))
end

PP10Norm=(IPIs10(2,:)./start10(2)'-1).*100;
PP10Norm(PP10Norm>49)=100;

for i=3
    plot(IPI,PP10Norm(1,:),'-','Color',colors(i,:))
end


plot(RC2(:,1),RC2(:,2),'ks')


axis([0 250 -50 50])
plot([0 500],[0 0],'LineWidth',2)
title('Recovery Cycle','FontSize',16)
xlabel('Inter Stimulus Interval (ms)','FontSize',16)
ylabel('%Change in Threshold','FontSize',16)

legend('Shin and Raymond 1991','PSO','Range','Sundt','Tigerholm','location','southeast')
fileName='Figures\RC_DRG_New.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

%% APD
figure
v2 = [0 1.35;4 1.35;4 3;0 3];
f2 = 1:4;
patch('Faces',f2,'Vertices',v2,'FaceColor','black','FaceAlpha',.1);
hold on
v2 = [4 1.5;8 1.5;8 2.6;4 2.6];
f2 = 1:4;
patch('Faces',f2,'Vertices',v2,'FaceColor','black','FaceAlpha',.1);
colors=parula(6);

% Optimized Autonomic C-fiber
plot([1],[2.3316],'o','Color',colors(1,:))
errorbar(1,2.3316,2.3316-2.3055,2.3332-2.3316,'Color',colors(1,:))

% Schild 94
data5=load('../Data_CFiber/Schild94\VoltageTrace\c_fiber_37_C_500_D_3_Type_1.dat');
data10=load('../Data_CFiber/Schild94\VoltageTrace\c_fiber_37_C_1000_D_3_Type_1.dat');
data20=load('../Data_CFiber/Schild94\VoltageTrace\c_fiber_37_C_1500_D_3_Type_1.dat');

plot([2],[FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1))],'o','Color',colors(1,:))
errorbar(2,FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1)),...
    FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1))-FindAPWidth(data5(1:end-1),linspace(0,20,length(data5)-1))...
    ,FindAPWidth(data20(1:end-1),linspace(0,20,length(data20)-1))-FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1)),'Color',colors(1,:))
% Schild 97
data5=load('../Data_CFiber/Schild97\VoltageTrace\c_fiber_37_C_500_D_4_Type_1.dat');
data10=load('../Data_CFiber/Schild97\VoltageTrace\c_fiber_37_C_1000_D_4_Type_1.dat');
data20=load('../Data_CFiber/Schild97\VoltageTrace\c_fiber_37_C_1500_D_4_Type_1.dat');

plot([3],[FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1))],'o','Color',colors(1,:))
errorbar(3,FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1)),...
    FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1))-FindAPWidth(data5(1:end-1),linspace(0,20,length(data5)-1))...
    ,FindAPWidth(data20(1:end-1),linspace(0,20,length(data20)-1))-FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1)),'Color',colors(1,:))


plot([5],[2.3892],'o','Color',colors(2,:))
errorbar(5,2.3892,2.3892-2.3293,2.4047-2.3892,'Color',colors(2,:))

% Sundt
data5=load('../Data_CFiber/Sundt\VoltageTrace\c_fiber_24_C_500_D_1_Type_1.dat');
data10=load('../Data_CFiber/Sundt\VoltageTrace\c_fiber_24_C_1000_D_1_Type_1.dat');
data20=load('../Data_CFiber/Sundt\VoltageTrace\c_fiber_24_C_1500_D_1_Type_1.dat');

plot([6],[FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1))],'o','Color',colors(1,:))
errorbar(6,FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1)),...
    FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1))-FindAPWidth(data5(1:end-1),linspace(0,20,length(data5)-1))...
    ,FindAPWidth(data20(1:end-1),linspace(0,20,length(data20)-1))-FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1)),'Color',colors(1,:))

% Tigerholm
data5=load('../Data_CFiber/Tigerholm\VoltageTrace\c_fiber_24_C_500_D_2_Type_1.dat');
data10=load('../Data_CFiber/Tigerholm\VoltageTrace\c_fiber_24_C_1000_D_2_Type_1.dat');
data20=load('../Data_CFiber/Tigerholm\VoltageTrace\c_fiber_24_C_1500_D_2_Type_1.dat');

plot([7],[FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1))],'o','Color',colors(1,:))
errorbar(7,FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1)),...
    FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1))-FindAPWidth(data5(1:end-1),linspace(0,20,length(data5)-1))...
    ,FindAPWidth(data20(1:end-1),linspace(0,20,length(data20)-1))-FindAPWidth(data10(1:end-1),linspace(0,20,length(data10)-1)),'Color',colors(1,:))


axis([0 8 0 8])
fileName='Figures\VoltageTrace_Final.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');
%% Threshold
figure
v2 = [0 0.015;4 0.015;4 0.15;0 0.15];
f2 = 1:4;
patch('Faces',f2,'Vertices',v2,'FaceColor','black','FaceAlpha',.1);
hold on

plot([1],[0.088623],'o','Color',colors(1,:))
errorbar(1,0.088623,0.088623-0.031616,0.162598-0.088623,'Color',colors(1,:))

plot([2],[0.020042],'o','Color',colors(2,:))
errorbar(2,0.020042,0.020042-0.031616,0.036758-0.020042,'Color',colors(2,:))

plot([3],[0.083649],'o','Color',colors(3,:))
errorbar(3,0.083771,0.083771-0.029625,0.153870-0.083771,'Color',colors(3,:))

ylim([0 0.2])
xticks([1 2 3])
xlim([0 4])
xticklabels({'PSO','Sundt','Tigerholm'})
set(gca,'XTickLabelRotation',15)
% xlabel('CV (m/s)')
ylabel('Intracellular Threshold (nA)')
% legend('Grundfest 1938','Iggo 1958','Paintal 1967','Sundt 2015','Tigerholm 2014','Rattay and Aberham 1993','PSO generated')
fileName='Figures\IntraThresh_Final_DRG.pdf';
% print(gcf,fileName,'-bestfit','-dpdf');

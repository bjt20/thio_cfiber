%{
Use PlotDigitizer.exe to pull the data points from published figures.
%}

clear
close all
format compact

scrnsz = get(0,'ScreenSize');
fntsz = 14;
lndwdth = 2;

subplot_rows = 2;
subplot_cols = 5;
figure
set(gcf,'outerposition',scrnsz)

%% Conduction speed
% ***** B-type

% Hursh 1939
% - Cat, excised nerve, 37.5degC
% - Compound recording
% - "The best curve relating velocity and diameter is a straight line.�
% - Measure velocity of fastest component in compound recording. Measure
% diameter of largest fiber histologically.
% - Only keep data up to 73 m/s, i.e. from hypogastric (11-41 m/s), cervical
% sympathetic (29-56), and vagus (67-73); some overlap with saphenous
% (65-82) and suralis (69-86).
load('Hursh1939_Fig2.mat')
tmp = Hursh1939_Fig2(:,2);
Hursh1939_Fig2 = Hursh1939_Fig2(tmp<73,:);

% Boyd 1979
% - Cat, cutaneous nerves in hind limb, in vivo
% - Compound recording
% - Group III muscle afferents (D = 3.96 � 0.61 um // CV = 4.40 � 0.23 m/s/um)
% - Group III cutaneous afferents (D = 4.59 � 0.45 um // CV = 4.38 � 0.06 m/s/um)
% - "Each point represents the matching of one peak in the afferent fibre
% diameter histogram of a nerve with the corresponding peak in the compound
% action potential of the same nerve, or of the mean diameter of the three
% largest fibres in a group with the conduction velocity derived from the
% latency of the start of the rising phase of the corresponding wave in the
% compound potential."
load('Boyd1979_Fig6.mat')

subplot(subplot_rows,subplot_cols,1)
plot(Hursh1939_Fig2(:,1),Hursh1939_Fig2(:,2),'kx')
hold on
plot(Boyd1979_Fig6(:,1),Boyd1979_Fig6(:,2),'ro')
legend('Hursh 1939 (6 m/s/\mum)','Boyd 1979 (4.5 m/s/\mum)','location','northwest')
xlabel('Fiber diameter (\mum)')
ylabel({'B-type','CV (m/s)'})
title('Conduction speed')

% ***** C-type
subplot(subplot_rows,subplot_cols,6)
% Sato 1985
% - Rat, in vivo(?)... "Each dissected nerve was placed into a paraffin oil
% bath which was closely maintained at a temperature of 37 � 0.3�C to
% avoid temperature effects on nerve conduction velocity (see ref. 6 for
% temperature effect)."
% - VN
% - Compound recording
% - "the vagus nerve had maximal unmyelinated conduction velocities between 1.4 and 1.6 m/s"
% - Can also see Fig 2B.
Sato1985 = [1 1.4; 1 1.6];
plot(Sato1985(:,1),Sato1985(:,2),'kx')
hold on

% Andrews 1980
% - Ferret, in vivo, VN (stomach mechanoreceptors)
% - Single unit recording
% - "mean conduction velocity was 0.91 � 0.21 m/sec", but not sure if they
% used SD or SE
% - Fig 1: 0.5 to 1.25 m/s
Andrews1980 = [2 0.5; 2 1.25];
plot(Andrews1980(:,1),Andrews1980(:,2),'ro')

ylim([0 2])
xlim([0.5 2.5])
xticks([1 2])
xticklabels({'Sato 1985','Andrews 1980'})
ylabel({'C-type','CV (m/s)'})

%% Strength-duration
xmin = 10;
xmax = 1000;
ymin = 0;
ymax = 10;

% ***** B-type
subplot(subplot_rows,subplot_cols,2)
% Woodbury 1990
% - Rat, cVN, in vivo
load('Woodbury1990_Fig2A_Btype.mat')
% Normalize to rheobase, estimated as threshold at largest thresh - ***** NOTE - Could estimate by fitting data to Ithresh equation instead
[~,ind]                       = sort(Woodbury1990_Fig2A_Btype(:,1));
Woodbury1990_Fig2A_Btype      = Woodbury1990_Fig2A_Btype(ind,:);
Woodbury1990_Fig2A_Btype(:,2) = Woodbury1990_Fig2A_Btype(:,2)./Woodbury1990_Fig2A_Btype(end,2);
plot(Woodbury1990_Fig2A_Btype(:,1),Woodbury1990_Fig2A_Btype(:,2),'kx')
hold on

% Table 1 , B fibers
Tch_min = 50;   % [us]
Tch_max = 200;  % [us]
PW = 10:1000;
Ith_normalized1 = 1./(1 - exp(-PW./Tch_min));
Ith_normalized2 = 1./(1 - exp(-PW./Tch_max));
plot(PW,Ith_normalized1,'k-')
h = plot(PW,Ith_normalized2,'k-');
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

% Castoro 2011
% - Dog, cVN, in vivo
% I50 traces
load('Castoro2011_Fig9B.mat')
for fiber_ind = 1:8
   tmp = Castoro2011_Fig9B{fiber_ind};
   % Normalize to rheobase, estimated as threshold at largest thresh - ***** NOTE - Could estimate by fitting data to Ithresh equation instead
   [~,ind]  = sort(tmp(:,1));
   tmp      = tmp(ind,:);
   tmp(:,2) = tmp(:,2)./tmp(end,2);
   h = plot(tmp(:,1),tmp(:,2),'r--');
   if fiber_ind ~= 1
      set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
   end
end

% Text, p. 67, B-fibers, I10
Tch_min = 40;   % [us]
Tch_max = 600;  % [us]
PW = 10:1000;
Ith_normalized1 = 1 + Tch_min./PW;
Ith_normalized2 = 1 + Tch_max./PW;
plot(PW,Ith_normalized1,'r-.')
h = plot(PW,Ith_normalized2,'r-.');
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

% Text, p. 67, B-fibers, I50
Tch_min = 100;   % [us]
Tch_max = 500;   % [us]
PW = 10:1000;
Ith_normalized1 = 1 + Tch_min./PW;
Ith_normalized2 = 1 + Tch_max./PW;
plot(PW,Ith_normalized1,'r-')
h = plot(PW,Ith_normalized2,'r-');
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

xlabel('PW (\mus)')
ylabel('Ith/Irh')
title('Strength-duration')
legend('Woodbury 1990 - anesthetized','Woodbury 1990 - min and max Tch',...
   'Castoro 2011 - indiv fibers','Castoro 2011 - min and max Tch for I10',...
   'Castoro 2011 - min and max Tch for I50')
xlim([xmin xmax])
ylim([ymin ymax])

% ***** C-type
subplot(subplot_rows,subplot_cols,7)
% Woodbury 1990
% - Rat, cVN, in vivo
load('Woodbury1990_Fig2A_Ctype.mat')
% Normalize to rheobase, estimated as threshold at largest thresh - ***** NOTE - Could estimate by fitting data to Ithresh equation instead
[~,ind]                       = sort(Woodbury1990_Fig2A_Ctype(:,1));
Woodbury1990_Fig2A_Ctype      = Woodbury1990_Fig2A_Ctype(ind,:);
Woodbury1990_Fig2A_Ctype(:,2) = Woodbury1990_Fig2A_Ctype(:,2)./Woodbury1990_Fig2A_Ctype(end,2);
plot(Woodbury1990_Fig2A_Ctype(:,1),Woodbury1990_Fig2A_Ctype(:,2),'kx')
hold on

load('Woodbury1990_Fig2B.mat')
% Normalize to rheobase, estimated as threshold at largest thresh - ***** NOTE - Could estimate by fitting data to Ithresh equation instead
[~,ind]                       = sort(Woodbury1990_Fig2A_Ctype(:,1));
Woodbury1990_Fig2B            = Woodbury1990_Fig2B(ind,:);
Woodbury1990_Fig2B(:,2)       = Woodbury1990_Fig2B(:,2)./Woodbury1990_Fig2B(end,2);
plot(Woodbury1990_Fig2B(:,1),Woodbury1990_Fig2B(:,2),'bx')

% Table 1, C1 & C2 fibers
Tch_min = 300;   % [us]
Tch_max = 650;  % [us]
PW = 50:700;
Ith_normalized1 = 1./(1 - exp(-PW./Tch_min));
Ith_normalized2 = 1./(1 - exp(-PW./Tch_max));
plot(PW,Ith_normalized1,'k-')
h = plot(PW,Ith_normalized2,'k-');
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

% Castoro 2011
% - Dog, cVN, in vivo
load('Koslow1973_Fig4.mat')
% Normalize to rheobase, estimated as threshold at largest thresh - ***** NOTE - Could estimate by fitting data to Ithresh equation instead
% Mean
[~,ind]                       = sort(Koslow1973_Fig4_mean(:,1));
Koslow1973_Fig4_mean            = Koslow1973_Fig4_mean(ind,:);
Koslow1973_Fig4_mean(:,2)       = Koslow1973_Fig4_mean(:,2)./Koslow1973_Fig4_mean(end,2);
plot(Koslow1973_Fig4_mean(:,1)*1000,Koslow1973_Fig4_mean(:,2),'ro')

% Upper bound
[~,ind]                       = sort(Koslow1973_Fig4_UB(:,1));
Koslow1973_Fig4_UB            = Koslow1973_Fig4_UB(ind,:);
Koslow1973_Fig4_UB(:,2)       = Koslow1973_Fig4_UB(:,2)./Koslow1973_Fig4_UB(end,2);
plot(Koslow1973_Fig4_UB(:,1)*1000,Koslow1973_Fig4_UB(:,2),'rs')

% Lower bound
[~,ind]                       = sort(Koslow1973_Fig4_LB(:,1));
Koslow1973_Fig4_LB            = Koslow1973_Fig4_LB(ind,:);
Koslow1973_Fig4_LB(:,2)       = Koslow1973_Fig4_LB(:,2)./Koslow1973_Fig4_LB(end,2);
h = plot(Koslow1973_Fig4_LB(:,1)*1000,Koslow1973_Fig4_LB(:,2),'rs');
set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');

xlabel('PW (\mus)')
ylabel('Ith/Irh')
legend('Woodbury 1990 - anesthetized','Woodbury 1990 - unanesthetized',...
   'Woodbury 1990 - min and max Tch','Koslow 1973 - mean','Koslow 1973 - mean+/-SD')
xlim([xmin xmax])
ylim([ymin ymax])

%% Threshold RC
% ***** C-type
subplot(subplot_rows,subplot_cols,3)
% Grundfest 1939
% One representative trace
load('Grundfest1939_Fig14.mat')
% Sort data points in order of ascending ISI
tmp = Grundfest1939_Fig14(:,1);
[~,ind] = sort(tmp);
Grundfest1939_Fig14 = Grundfest1939_Fig14(ind,:);
% Redefine y axis:
% 100% excitability = 0% change in threshold
% 0% excitability   = inf threshold
% Decreased excitability = increase in threshold
tmp = Grundfest1939_Fig14(:,2);
Grundfest1939_Fig14(:,2) = 100.*(100./tmp - 1);
% Plot
% plot(Grundfest1939_Fig14(:,1),Grundfest1939_Fig14(:,2),'kx')
plot(Grundfest1939_Fig14(2:end,1),Grundfest1939_Fig14(2:end,2),'kx')
hold on

xlabel('ISI (ms)')
ylabel('% change in threshold')
legend('Grundfest 1939, Fig 14 (inf thresh at 1 ms)')
title('Threshold recovery cycle')

% ***** C-type
subplot(subplot_rows,subplot_cols,8)
% Grundfest 1938
% Two representative traces
load('Grundfest1938_Fig10.mat')
% Sort data points in order of ascending ISI
tmp = Grundfest1938_Fig10(:,1);
[~,ind] = sort(tmp);
Grundfest1938_Fig10 = Grundfest1938_Fig10(ind,:);
% Redefine y axis:
% 100% excitability = 0% change in threshold
% 0% excitability   = inf threshold
% Decreased excitability = increase in threshold
tmp = Grundfest1938_Fig10(:,2);
Grundfest1938_Fig10(:,2) = 100.*(100./tmp - 1);
% Plot
plot(Grundfest1938_Fig10(:,1),Grundfest1938_Fig10(:,2),'kx')
hold on

load('Grundfest1938_Fig11.mat')
% Sort data points in order of ascending ISI
tmp = Grundfest1938_Fig11(:,1);
[~,ind] = sort(tmp);
Grundfest1938_Fig11 = Grundfest1938_Fig11(ind,:);
% Redefine y axis:
% 100% excitability = 0% change in threshold
% 0% excitability   = inf threshold
% Decreased excitability = increase in threshold
tmp = Grundfest1938_Fig11(:,2);
Grundfest1938_Fig11(:,2) = 100.*(100./tmp - 1);
% Plot
plot(Grundfest1938_Fig11(:,1),Grundfest1938_Fig11(:,2),'ks')

% xlim([0 100])
xlabel('ISI (ms)')
ylabel('% change in threshold')
legend('Grundfest 1938, Fig 10','Grundfest 1938, Fig 11','location','southeast')

%% AP duration
% ***** B-type
subplot(subplot_rows,subplot_cols,4)
% Grundfest
Grundfest1939 = [0 1.2];
plot(Grundfest1939(:,1),Grundfest1939(:,2),'b*')
hold on

% Iggo 1958
load('Iggo1958_Fig7.mat')
plot(Iggo1958_Fig7_Bfibers(:,1),Iggo1958_Fig7_Bfibers(:,2),'kx')

% Paintal 1966 & 1967
%{
"In order to eliminate any guessing about the end of the spike, a difficulty
experienced by earlier workers (e.g. Blair & Erlanger, 1933), a straight
line was drawn over the down-slope of the mono- phasic spike and the
duration reckoned as the interval between the beginning of the spike (which
was easy to define) to the point where the superimposed straight line cut
the base line (Fig. 1B)."
%}
load('Paintal1966_Fig2B.mat')
plot(Paintal1966_Fig2B(:,1),Paintal1966_Fig2B(:,2),'ro')

load('Paintal1967_Fig1.mat')
plot(Paintal1967_Fig1_Bfibers(:,1),Paintal1967_Fig1_Bfibers(:,2),'gs')

ylim([0 3])
xlabel('CV (m/s)')
ylabel('Spike duration (ms)')
title('Spike duration')
legend('Grundfest 1939','Iggo 1958','Paintal 1966','Paintal 1967')

% ***** C-type
subplot(subplot_rows,subplot_cols,9)
% Grundfest
Grundfest1938 = [0 2; 0 2.5];
plot(Grundfest1938(:,1),Grundfest1938(:,2),'b*')
hold on

% Iggo 1958
load('Iggo1958_Fig7.mat')
plot(Iggo1958_Fig7_Cfibers(:,1),Iggo1958_Fig7_Cfibers(:,2),'kx')

% Paintal 1967 - same approach as Paintal 1966
%{
Paintal 1966: "In order to eliminate any guessing about the end of the spike, a difficulty
experienced by earlier workers (e.g. Blair & Erlanger, 1933), a straight
line was drawn over the down-slope of the mono- phasic spike and the
duration reckoned as the interval between the beginning of the spike (which
was easy to define) to the point where the superimposed straight line cut
the base line (Fig. 1B)."
%}
load('Paintal1967_Fig1.mat')
plot(Paintal1967_Fig1_Cfibers(:,1),Paintal1967_Fig1_Cfibers(:,2),'gs')

ylim([0 3])
xlabel('CV (m/s)')
ylabel('Spike duration (ms)')
legend('Grundfest 1938','Iggo 1958','Paintal 1967')

%% Absolute refractory period
% ***** B-type
subplot(subplot_rows,subplot_cols,5)
% Grundfest 1939, 1940
% - Cat hypogastric
Grundfest1939 = [1 1.1; 1 1.5];
plot(Grundfest1939(:,1),Grundfest1939(:,2),'kx')

ylim([0 3])
xlim([0.5 1.5])
xticks(1)
xticklabels({'Grundfest 1939'})
ylabel('Abs refractory period (ms)')
title('Absolute refractory period')

% ***** C-type
subplot(subplot_rows,subplot_cols,10)
% Grundfest 1938, 1940
% - Cat hypogastric
Grundfest1938 = [1 1.8; 1 2];
plot(Grundfest1938(:,1),Grundfest1938(:,2),'kx')

ylim([0 3])
xlim([0.5 1.5])
xticks(1)
xticklabels({'Grundfest 1938'})
ylabel('Abs refractory period (ms)')
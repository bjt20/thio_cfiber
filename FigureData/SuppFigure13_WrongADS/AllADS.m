%% ADS Autonomic
Diam=[1];
Freq=[2];
Temp=[37];
Batch=[2 3 11 13 15 18 19 22 25 35 43 44 49];
ID=[37 31 39 39 33 34 34 14 39 14 33 14 34];
load ../ValidationData/ADSInVivo.mat
figure
hold on
errorbar(savetime(:,1),mean(savedeltaCV(:,2:4),2),std(savedeltaCV(:,2:4)')./sqrt(3))
colors=parula(10);
for f=Freq
    for d=Diam
        for t=Temp
            for i = 1:length(Batch)
                data=[];
                name=['ADS\AAT' num2str(Batch(i)) '\c_fiber_' num2str(d.*1000) '_D_5_Type_' num2str(f.*10) '_Freq_' num2str(floor(1/f)) '_delay_' num2str(ID(i)) '_' num2str(t) '_Temp_VoltageTraceS1_Full_2.dat'];
                if exist(name)
                    cFiberT1=load(name);
                    cFiberT2=load(['ADS\AAT' num2str(Batch(i)) '\c_fiber_' num2str(d.*1000) '_D_5_Type_' num2str(f.*10) '_Freq_' num2str(floor(1/f)) '_delay_' num2str(ID(i)) '_' num2str(t) '_Temp_VoltageTraceS2_Full_2.dat']);
                    if length(cFiberT1)>0
                        hold on
                        if Batch(i)==44
                            plot(linspace(0,180,length(cFiberT2(1:3:end))),100.*(2.5./(cFiberT2(1:3:end)-cFiberT1(1:3:end))./(2.5./(cFiberT2(1)-cFiberT1(1)))-1),'LineWidth',2,'Color','r')
                        else
                            plot(linspace(0,180,length(cFiberT2(1:3:end))),100.*(2.5./(cFiberT2(1:3:end)-cFiberT1(1:3:end))./(2.5./(cFiberT2(1)-cFiberT1(1)))-1),'LineWidth',1,'Color',[0.75 0.75 0.75])
                        end
                        axis([0 180 -40 1])
                        data=[data 100.*(2.5./(cFiberT2(1:3:end)-cFiberT1(1:3:end))./(2.5./(cFiberT2(1)-cFiberT1(1)))-1)];
                    end
                end
            end
        end
    end
    legend('Experimental 2 Hz','0.5 Hz','1 Hz','2 Hz','4 Hz')
    xlabel('Time (s)')
    ylabel('% change in conduction velocity')
    fileName=['Figures\ADS_' num2str(d*10) '_' num2str(Temp) '.pdf'];
    print(gcf,fileName,'-bestfit','-dpdf');
end
%% ADS Cutaneous
Diam=[1];
Freq=[2];
Temp=[37];
load ../ValidationData/ADS_Experimental_LTH.mat
runs=[1 2 3 5 8 9 11 12 13 14 15 18 19 20];
IDs=[22 7 38 30 7 33 39 33 13 18 31 30 14 34];
figure
hold on
plot(ADS_Experimental_LTH(:,1),(1-100./(100-ADS_Experimental_LTH(:,2))).*100,'o')
for it=1:length(runs)
    run=runs(it);
    ID=IDs(it);
    
    
    for f=Freq
        data=[];
        for d=Diam
            for t=Temp
                name=['ADS\ACT' num2str(run) '\c_fiber_' num2str(d.*1000) '_D_5_Type_' num2str(f.*10) '_Freq_' num2str(floor(1/f)) '_delay_' num2str(ID) '_' num2str(t) '_Temp_VoltageTraceS1_Full_2.dat'];
                if exist(name)
                    cFiberT1=load(name);
                    cFiberT2=load(['ADS\ACT' num2str(run) '\c_fiber_' num2str(d.*1000) '_D_5_Type_' num2str(f.*10) '_Freq_' num2str(floor(1/f)) '_delay_' num2str(ID) '_' num2str(t) '_Temp_VoltageTraceS2_Full_2.dat']);
                    hold on
                    if run==5
                        plot(linspace(0,180,length(cFiberT2(1:3:end))),100.*(2.5./(cFiberT2(1:3:end)-cFiberT1(1:3:end))./(2.5./(cFiberT2(1)-cFiberT1(1)))-1),'LineWidth',2,'Color','r')
                    else
                        plot(linspace(0,180,length(cFiberT2(1:3:end))),100.*(2.5./(cFiberT2(1:3:end)-cFiberT1(1:3:end))./(2.5./(cFiberT2(1)-cFiberT1(1)))-1),'LineWidth',1,'Color',[0.75 0.75 0.75])
                    end
                    axis([0 180 -40 1])
                    data=[data 100.*(2.5./(cFiberT2(1:3:end)-cFiberT1(1:3:end))./(2.5./(cFiberT2(1)-cFiberT1(1)))-1)];
                end
            end
        end
        legend('Experimental 2 Hz','0.5 Hz','1 Hz','2 Hz','4 Hz')
        xlabel('Time (s)')
        ylabel('% change in conduction velocity')
        fileName=['Figures\ADS_' num2str(d*10) '_' num2str(Temp) '_' num2str(run) '_' num2str(ID) '_new.pdf'];
        print(gcf,fileName,'-bestfit','-dpdf');
    end
end
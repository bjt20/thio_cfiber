%% CV Cutaneous
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
CVs10=[];
cv_10_20=[load(['../Data_CFiber/Cutaneous/CV/c_fiber_CV_37_C_1000_D_5_Type_' num2str(30) '.dat']) load(['../Data_CFiber/Cutaneous/CV/c_fiber_CV_37_C_1500_D_5_Type_' num2str(30) '.dat'])];
for i=[(100:100:1500)+1 (100:100:1500)+2]
    CVs10=[CVs10 load(['../Data_CFiber/Cutaneous/CV/c_fiber_CV_37_C_1000_D_5_Type_' num2str(i) '.dat'])];
end
CVs10=(reshape(CVs10,15,2)-cv_10_20(1))./cv_10_20(1);
figure
barh(-1:-1:-15,CVs10.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Cutaneous_CV_10.pdf','-dpdf','-painters')

%% APD Cutaneous
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
APDs10=[];
temp1=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_24_C_1000_D_5_Type_' num2str(30) '.dat']);
time=linspace(0,20,length(temp1));
temp2=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_24_C_1500_D_5_Type_' num2str(30) '.dat']);
APD_10_20=[FindAPWidth(temp1,time),FindAPWidth(temp2,time)];
for i=[(100:100:1500)+1 (100:100:1500)+2]
    temp1=load(['../Data_CFiber/Cutaneous/VoltageTrace/c_fiber_24_C_1000_D_5_Type_' num2str(i) '.dat']);
    APDs10=[APDs10 FindAPWidth(temp1,time(1:length(temp1)))];
end
APDs10=(reshape(APDs10,15,2)-APD_10_20(1))./APD_10_20(1);
figure
barh(-1:-1:-15,APDs10.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Cutaneous_APD_10.pdf','-dpdf','-painters')

%% SD Cutaneous
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
SDs10=[];
PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 10];
data10=[];
for i=1:length(PW)
    data10=[data10 load(['../Data_CFiber/Cutaneous/SD/c_fiber_Thresh_33_C_1000_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(30) '.dat'])];
end
SD_10(1)=findChronaxie(PW,data10);

for i=[(100:100:1500)+1 (100:100:1500)+2]
    data10=[];
    for j=1:length(PW)
        data10=[data10 load(['../Data_CFiber/Cutaneous/SD/c_fiber_Thresh_33_C_1000_D_' num2str(PW(j).*1000) '_Duration_5_Type_' num2str(i) '.dat'])];
    end
    SDs10=[SDs10 findChronaxie(PW,data10)];
end
SDs10=(reshape(SDs10,15,2)-SD_10(1))./SD_10(1);
figure
barh(-1:-1:-15,SDs10.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Cutaneous_SD_10.pdf','-dpdf','-painters')

%% RC Cutaneous
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
PPs10=[];
load ../ValidationData/RCData.mat

IPI=1:2:250;
IPIs10=[];
for n=30
    ct=1;
    start10=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_0_Delay_5_Type_' num2str(n) '.dat']);
    for i=IPI
        if isempty(load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_' num2str(i) '_Delay_5_Type_' num2str(n) '.dat']))
            IPIs10(1,ct)=100;
        else
            IPIs10(1,ct)=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_' num2str(i) '_Delay_5_Type_' num2str(n) '.dat']);
        end
        ct=ct+1;
    end
end

PP10Norm=(IPIs10./start10'-1).*100;
PP10Norm(PP10Norm>49)=100;

RC=[RC1;RC2];
RC(RC(:,1)>250,:)=[];
PP_10=sqrt(sum((RC(:,2)-interp1(IPI,PP10Norm,RC(:,1))).^2));

for i=[(100:100:1500)+1 (100:100:1500)+2]
    IPIs10=[];
    ct=1;
    if isempty(load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_0_Delay_5_Type_' num2str(i) '.dat']))
        start10=100;
    else
        start10=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_0_Delay_5_Type_' num2str(i) '.dat']);
    end

    for j=IPI
        if isempty(load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_' num2str(j) '_Delay_5_Type_' num2str(i) '.dat']))
            IPIs10(1,ct)=100;
        else
            IPIs10(1,ct)=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_33_C_1000_D_100_Duration_' num2str(j) '_Delay_5_Type_' num2str(i) '.dat']);
        end
        ct=ct+1;
    end
    
    PP10Norm=(IPIs10./start10'-1).*100;
    PP10Norm(PP10Norm>49)=100;
    
    PPs10=[PPs10 sqrt(sum((RC(:,2)-interp1(IPI,PP10Norm,RC(:,1))).^2))];
end
PPs10=(reshape(PPs10,15,2)-PP_10(1))./PP_10(1);
figure
barh(-1:-1:-15,PPs10.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
print('Figures/Cutaneous_RC_10.pdf','-dpdf','-painters')

%% Threshold ../Data_CFiber/Cutaneous
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
PPs5=[];
PPs20=[];

IPI=1:2:250;
IPIs5=[];
IPIs20=[];
for n=30
    ct=1;
    start5Orig=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_24_C_1000_D_10000_Duration_0_Delay_5_Type_' num2str(n) '.dat']);
    start20Orig=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_24_C_1500_D_10000_Duration_0_Delay_5_Type_' num2str(n) '.dat']);
end

for i=[(100:100:1500)+1 (100:100:1500)+2]
    IPIs5=[];
    IPIs20=[];
    ct=1;
    start5=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_24_C_1000_D_10000_Duration_0_Delay_5_Type_' num2str(i) '.dat']);
    start20=load(['../Data_CFiber/Cutaneous/Thresh/c_fiber_Thresh_24_C_1500_D_10000_Duration_0_Delay_5_Type_' num2str(i) '.dat']);

    PPs5=[PPs5 start5];
    PPs20=[PPs20 start20];
end
PPs5=(reshape(PPs5,15,2)-start5Orig)./start5Orig;
figure
barh(-1:-1:-15,PPs5.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Cutaneous_Thresh_10.pdf','-dpdf','-painters')
% PPs20=(reshape(PPs20,15,2)-start20Orig)./start20Orig;
% figure
% barh(-1:-1:-15,PPs20.*100)
% axis([-50 50 -16 0])
% yticks(-15:-1)
% yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Cutaneous_Thresh_15.pdf','-dpdf','-painters')






addpath ../../AnalysisFtns
%% CV ../Data_CFiber/Autonomic
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
CVs10=[];
cv_5_20=[load(['../Data_CFiber/Autonomic/CV/c_fiber_CV_37_C_500_D_5_Type_' num2str(14) '.dat']) load(['../Data_CFiber/Autonomic/CV/c_fiber_CV_37_C_1000_D_5_Type_' num2str(14) '.dat']) load(['../Data_CFiber/Autonomic/CV/c_fiber_CV_37_C_2000_D_5_Type_' num2str(14) '.dat'])];
for i=[(100:100:1500)+1 (100:100:1500)+2]
    CVs10=[CVs10 load(['../Data_CFiber/Autonomic/CV/c_fiber_CV_37_C_1000_D_5_Type_' num2str(i) '.dat'])];
end
CVs10=(reshape(CVs10,15,2)-cv_5_20(2))./cv_5_20(2);
figure
barh(-1:-1:-15,CVs10.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Autonomic_CV_10.pdf','-dpdf','-painters')
[B,I]=sort(sum(abs(CVs10),2));


%% APD Autonomic
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
APDs10=[];
temp1=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_37_C_1000_D_5_Type_' num2str(14) '.dat']);
time=linspace(0,20,length(temp1));
APD_5_20=[FindAPWidth(temp1,time)];
for i=[(100:100:1500)+1 (100:100:1500)+2]
    temp1=load(['../Data_CFiber/Autonomic/VoltageTrace/c_fiber_37_C_1000_D_5_Type_' num2str(i) '.dat']);
    APDs10=[APDs10 FindAPWidth(temp1,time(1:length(temp1)))];
end
APDs10=(reshape(APDs10,15,2)-APD_5_20(1))./APD_5_20(1);
figure
barh(-1:-1:-15,APDs10.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Autonomic_APD_10.pdf','-dpdf','-painters')

%% SD Autonomic
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
SDs10=[];
PW = [0.02 0.035 0.05 0.075 0.1 0.2 0.5 1 10];
data10=[];
for i=1:length(PW)
    data10=[data10 load(['../Data_CFiber/Autonomic/SD/c_fiber_Thresh_37_C_1000_D_' num2str(PW(i).*1000) '_Duration_5_Type_' num2str(14) '.dat'])];
end
SD_5_20(1)=findChronaxie(PW,data10);

for i=[(100:100:1500)+1 (100:100:1500)+2]
    data10=[];
    for j=1:length(PW)
        data10=[data10 load(['../Data_CFiber/Autonomic/SD/c_fiber_Thresh_37_C_1000_D_' num2str(PW(j).*1000) '_Duration_5_Type_' num2str(i) '.dat'])];
    end
    SDs10=[SDs10 findChronaxie(PW,data10)];
end
SDs10=(reshape(SDs10,15,2)-SD_5_20(1))./SD_5_20(1);
figure
barh(-1:-1:-15,SDs10.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Autonomic_SD_10.pdf','-dpdf','-painters')

%% RC Autonomic
labels={'Ra','I_{NaK Pump}','NaCaX','KCa_{SK}','K_A1.4','K_A3.4','K_V2.1','HCN','K_V7','Ca_V2.2','Ca_V1.2','KCa_{BK}','Na_V1.9','Na_V1.8','Na_V1.7'};
PPs5_20=[];
load('..\ValidationData\Grundfest1938_Fig10.mat')
tmp = Grundfest1938_Fig10(:,1);
[~,ind] = sort(tmp);
Grundfest1938_Fig10 = Grundfest1938_Fig10(ind,:);
tmp = Grundfest1938_Fig10(:,2);
Grundfest1938_Fig10(:,2) = 100.*(100./tmp - 1);

IPI=1:2:250;
IPIs10=[];
start10=[];
for n=14
    ct=1;
    start10(1)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_0_Delay_5_Type_' num2str(n) '.dat']);
    for i=IPI
        if isempty(load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(i) '_Delay_5_Type_' num2str(n) '.dat']))
            IPIs10(1,ct)=100;
        else
            IPIs10(1,ct)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(i) '_Delay_5_Type_' num2str(n) '.dat']);
        end
        
        ct=ct+1;
    end
end

PP10Norm=(IPIs10./start10'-1).*100;
PP10Norm(PP10Norm>49)=100;

PP_5_20=sqrt(sum((Grundfest1938_Fig10(:,2)-interp1(IPI,PP10Norm,Grundfest1938_Fig10(:,1))).^2));

for i=[(100:100:1500)+1 (100:100:1500)+2]
    IPIs10=[];
    ct=1;
    start10=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_0_Delay_5_Type_' num2str(i) '.dat']);
    for j=IPI
        if isempty(load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(j) '_Delay_5_Type_' num2str(i) '.dat']))
            IPIs10(1,ct)=100;
        else
            IPIs10(1,ct)=load(['../Data_CFiber/Autonomic/Thresh/c_fiber_Thresh_37_C_1000_D_100_Duration_' num2str(j) '_Delay_5_Type_' num2str(i) '.dat']);
        end
        ct=ct+1;
    end
    
    PP10Norm=(IPIs10./start10'-1).*100;
    PP10Norm(PP10Norm>49)=100;

    PPs5_20=[PPs5_20 sqrt(sum((Grundfest1938_Fig10(:,2)-interp1(IPI,PP10Norm,Grundfest1938_Fig10(:,1))).^2))];
end
PPs5_20=(reshape(PPs5_20,15,2)-PP_5_20(1))./PP_5_20(1);
figure
barh(-1:-1:-15,PPs5_20.*100)
axis([-50 50 -16 0])
yticks(-15:-1)
yticklabels(labels)
% print('Figures/Sensitivity_../Data_CFiber/Autonomic_RC_5_20.pdf','-dpdf','-painters')





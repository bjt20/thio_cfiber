
PSOTypes={'ACT','AAT'};
which=[2 1];
for type=2%:2
    for i=21:50
        cd([PSOTypes{type} num2str(i)])
%         cd([PSOTypes{type} num2str(i) '/Elect'])
%         latestFile=getlatestfile('.');
%         j=1;
%         while ~strcmp(['Swarm' num2str(j) '.mat'],latestFile) & ~strcmp(['Outcomes' num2str(j) '.mat'],latestFile)
%             j=j+1;
%         end
%         load(['Swarm' num2str(j-1) '.mat'])
%         binarypotential=bp;
%         potential=p;
%         save(['Outcomes' num2str(j) '.mat'],'binarypotential','potential','swarm')
% %         copyfile(['Swarm' num2str(j) '.mat'],['Swarm' num2str(2) '.mat'])
% %         copyfile(['Outcomes' num2str(j) '.mat'],['Outcomes' num2str(2) '.mat'])
%         cd('..')
        fid = fopen('Batch.slurm','w');
        fprintf(fid,'#!/bin/bash\n');
        fprintf(fid,'#\n');
        fprintf(fid,'#SBATCH --output=cpso.out\n');
        fprintf(fid,'#SBATCH --open-mode=append\n');
        fprintf(fid,'#SBATCH --job-name=CPSO\n');
        fprintf(fid,'#SBATCH --mem=120000\n');
        fprintf(fid,'#SBATCH --error=cpso.err\n');
%         fprintf(fid,'#SBATCH --nodelist=dcc-wmglab-38,dcc-wmglab-49,dcc-wmglab-54,dcc-wmglab-55,dcc-wmglab-56,dcc-wmglab-57,dcc-wmglab-58,dcc-wmglab-59,dcc-wmglab-60,dcc-wmglab-61,dcc-wmglab-62,dcc-wmglab-63,dcc-wmglab-64,dcc-wmglab-65,dcc-wmglab-66,dcc-wmglab-67\n');
        fprintf(fid,'#SBATCH -N 1\n');
        fprintf(fid,'#SBATCH -n 61\n');
        fprintf(fid,'#SBATCH -p wmglab\n\n');
        fprintf(fid,'module unload Neuron\n');
        fprintf(fid,'module load Neuron/7.6.2\n');
        fprintf(fid,'echo $SLURM_JOB_ID\n');
        fprintf(fid,'#input variables are "version,analogversion,modelType,startingIteration,smartProp"\n');
        fprintf(fid,['/opt/apps/rhel7/matlabR2019a/bin/matlab -nodisplay -singleCompThread -r "RunNewCPSO(' num2str(which(type)) ',1,5,' num2str(1) ',0)"']);
        fclose(fid);
        %submit the job and catch the job id
        [MasterID,~] = evalc(sprintf('system(''sbatch Batch.slurm'')'));
        cd('..')
    end
end

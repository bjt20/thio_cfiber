# Reverse-engineered models of autonomic and cutanous C-fibers

We developed methods to create isoform-specific ion channel models and to reverse-engineer the expression of ion channels within C-fiber axons. Additionally, we created models of 12 different isoform-specific ion channel models and two C-fiber axon, autonomic and cutaneous.

Funding: This work is supported by NIH SPARC OT2 025340. 

## PSO requirements
- matlabR2019a
- NEURON v7.6
- SLURM cluster (with ≥61 CPUs/node and ≥120 GB/node)

## Data Overview
### Figure Data
All data and code to recreate the figures in the manuscript are located in the `/FigureData` directory. Each folder in the directory is labeled based on the figure it recreates and the figure can be recreated by running the matlab scripts.
### Mechanism Data
All data for the .mod files used in the PSO generated C-fiber models and other published C-fiber models are found in the `/AllMechanisms` directory.
### C-fiber code
Code to run the PSOs are found in the `/RunPSO` directory.
The PSO code is found in the `/Code_PSO` directory.
The code to run other published C-fiber models is found in the `/Code_OtherFibers` directory.
The code to aide in visualization and analysis is found in the `/AnalysisFtns` directory.

## Running the code
### Running the PSO
The codebase requries a SLURM-based computing cluster (see requriements section) and is broken into independent sections. 

0. Install necessary code and dependencies. 
    * Clone this repository and download dependencies (refer to requirements section).
1. Create a directory called ACT{Number} or AAT{Number} for a cutaneous or autonomic C-fiber model in the `/RunPSO` directory.
2. Put the code from `/Code_PSO` into the newly created directory.
3. Edit the BatchAllPSOs.m file to run the PSOs that you want.
4. Compile the .mod files using the nrnivmodl command and copy the special file into the created directory.
5. Run Batch.slurm in `/RunPSO` to run the PSOs that you defined.
6. Monitor the PSOs using the Monitor.slurm and MonitorPSOs.m scripts in `/RunPSO`
7. After the PSOs finish run Batch3.slurm to characterize the conduction responses of the fibers and Batch4.slurm to characterize the ADS of the fibers in the created directory.
8. Similarly, you can characterize the other fiber models by copying the special file into the `Code_OtherFibers` directory and running the Batch{Number}.slurm scripts.

## Authors 
- Brandon J. Thio,  Department of Biomedical Engineering, Duke University, Durham, NC, 27708, USA
- Nathan D. Titus, Department of Biomedical Engineering, Duke University, Durham, NC, 27708, USA
- Nicole A. Pelot, Department of Biomedical Engineering, Duke University, Durham, NC, 27708, USA
- Warren M. Grill,  Department of Biomedical Engineering, Electrical and Computer Engineering (secondary), Department of Neurobiology (secondary), Department of Neurosurgery (secondary), Duke University, Durham, NC, 27708, USA

## Citation
If you use any part of this code for your work, please cite our paper.

## License
The copyrights of this software are owned by Duke University. As such, two licenses to this software are offered:
1. An open-source license under the GPLv2 license for non-commercial use.
2. A custom license with Duke University, for commercial use without the GPLv2 license restrictions. 
 
As a recipient of this software, you may choose which license to receive the code under. Outside contributions to the Duke-owned code base cannot be accepted unless the contributor transfers the copyright to those changes over to Duke University.
To enter a custom license agreement without the GPLv2  license restrictions, please contact the Duke Office for Translation & Commercialization (OTC) (https://olv.duke.edu/) at otcquestions@duke.edu with
reference to “OTC File No. 7982 in your email. 
 
Please note that this software is distributed AS IS, WITHOUT ANY WARRANTY; and without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

